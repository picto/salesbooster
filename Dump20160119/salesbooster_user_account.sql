-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: salesbooster
-- ------------------------------------------------------
-- Server version	5.7.9-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `user_account`
--

DROP TABLE IF EXISTS `user_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_account` (
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `email_pass` varchar(100) DEFAULT NULL,
  `roles` varchar(200) NOT NULL,
  PRIMARY KEY (`username`),
  UNIQUE KEY `UK_castjbvpeeus0r8lbpehiu0e4` (`username`),
  KEY `usernameIndex` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_account`
--

LOCK TABLES `user_account` WRITE;
/*!40000 ALTER TABLE `user_account` DISABLE KEYS */;
INSERT INTO `user_account` VALUES ('DOS','202cb962ac59075b964b07152d234b70','Anne T. Knowles','stm.sales.booster@gmail.com','stmsb196','DSALES'),('Manager','202cb962ac59075b964b07152d234b70','Vladimir H. Lancaster','stm.sales.booster@gmail.com','stmsb196','BM'),('SA1','202cb962ac59075b964b07152d234b70','Colorado N. Burgess','stm.sales.booster@gmail.com','stmsb196','SA'),('SA2','202cb962ac59075b964b07152d234b70','Wyoming Q. Mejia','stm.sales.booster@gmail.com','stmsb196','SA'),('SA3','202cb962ac59075b964b07152d234b70','Maxine N. Compton','stm.sales.booster@gmail.com','stmsb196','SA'),('SA4','202cb962ac59075b964b07152d234b70','Shelly T. Becker','stm.sales.booster@gmail.com','stmsb196','SA'),('SA5','202cb962ac59075b964b07152d234b70','Harlan M. Randall','stm.sales.booster@gmail.com','stmsb196','SA'),('sa6','202cb962ac59075b964b07152d234b70','Mamunur Rashid','stm.sales.booster@gmail.com','stmsb196','SA'),('SysAdmin','202cb962ac59075b964b07152d234b70','Kylie U. Case','stm.sales.booster@gmail.com','stmsb196','SYS_ADMIN');
/*!40000 ALTER TABLE `user_account` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-19 11:53:02
