-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: salesbooster
-- ------------------------------------------------------
-- Server version	5.7.9-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `productrelation`
--

DROP TABLE IF EXISTS `productrelation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productrelation` (
  `productid` bigint(20) NOT NULL,
  `relatedproductid` bigint(20) NOT NULL,
  `distnct` bigint(20) NOT NULL,
  `ids` longtext,
  `times` bigint(20) NOT NULL,
  PRIMARY KEY (`productid`,`relatedproductid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productrelation`
--

LOCK TABLES `productrelation` WRITE;
/*!40000 ALTER TABLE `productrelation` DISABLE KEYS */;
INSERT INTO `productrelation` VALUES (3,4,3,'16;3;69',4),(3,5,1,'14',1),(3,6,6,'16;3;69;39;6;7',12),(3,7,3,'3;17;42',4),(3,12,1,'17',1),(3,13,6,'3;17;42;14;39;7',11),(4,3,3,'16;3;69',4),(4,6,7,'16;3;1;13;25;67;69',12),(4,7,2,'3;25',3),(4,8,1,'13',1),(4,12,1,'3',1),(4,13,2,'3;25',4),(5,3,1,'14',1),(5,13,1,'14',1),(6,3,6,'16;3;69;39;6;7',12),(6,4,7,'16;3;1;13;25;67;69',12),(6,7,2,'3;25',3),(6,8,1,'13',1),(6,12,1,'3',1),(6,13,4,'3;25;39;7',10),(7,3,3,'3;17;42',4),(7,4,2,'3;25',3),(7,6,2,'3;25',3),(7,9,1,'105',1),(7,11,1,'6',1),(7,12,3,'17;95;6',3),(7,13,8,'3;17;42;25;48;38;95;105',9),(7,14,1,'95',1),(8,4,1,'13',1),(8,6,1,'13',1),(9,7,1,'105',1),(9,13,1,'105',1),(11,7,1,'6',1),(11,12,1,'6',1),(12,3,1,'17',1),(12,4,1,'3',1),(12,6,1,'3',1),(12,7,3,'17;95;6',3),(12,11,1,'6',1),(12,13,3,'3;17;95',3),(12,14,2,'95;4',2),(13,3,6,'3;17;42;14;39;7',11),(13,4,2,'3;25',4),(13,5,1,'14',1),(13,6,4,'3;25;39;7',10),(13,7,8,'3;17;42;25;48;38;95;105',9),(13,9,1,'105',1),(13,12,3,'3;17;95',3),(13,14,1,'95',1),(14,7,1,'95',1),(14,12,2,'95;4',2),(14,13,1,'95',1);
/*!40000 ALTER TABLE `productrelation` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-19 11:53:03
