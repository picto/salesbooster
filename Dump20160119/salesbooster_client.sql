-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: salesbooster
-- ------------------------------------------------------
-- Server version	5.7.9-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `version` bigint(20) NOT NULL,
  `address` varchar(255) NOT NULL,
  `annualrevenue` bigint(20) NOT NULL,
  `candelegate` bit(1) NOT NULL,
  `contactname` varchar(255) DEFAULT NULL,
  `contactno` varchar(255) DEFAULT NULL,
  `contacttitle` varchar(255) DEFAULT NULL,
  `decisiontime` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `employeesize` bigint(20) NOT NULL,
  `lat` varchar(255) DEFAULT NULL,
  `lng` varchar(255) DEFAULT NULL,
  `givenleadscore` bigint(20) NOT NULL,
  `gmapid` varchar(255) DEFAULT NULL,
  `industry` varchar(255) DEFAULT NULL,
  `leadscore` bigint(20) NOT NULL,
  `loyalty` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `placeid` varchar(255) DEFAULT NULL,
  `priority` varchar(255) NOT NULL,
  `sadder` varchar(255) NOT NULL,
  `srepresentative` varchar(255) DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  `website` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_k2ff7ow7fw2d52sphy793v4fs` (`gmapid`),
  UNIQUE KEY `UK_6ywcpycmmji1lvra6ya1b3scd` (`placeid`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client`
--

LOCK TABLES `client` WRITE;
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
INSERT INTO `client` VALUES (1,'2016-01-19 09:09:18','2016-01-19 09:40:35',0,'100 Neo Tiew Rd, Singapore 719026',0,'\0',NULL,NULL,NULL,NULL,NULL,0,'1.418319','103.71685300000001',2,'ChIJcfqTm-0N2jERogSAub4V7rM','restaurant',2,0,'Poison Ivy Bistro','+65 6898 5001','poison+ivy+bistro','LOW','SA1','SA5','ACTIVE','http://bollywoodveggies.com/'),(2,'2016-01-19 09:09:19','2016-01-19 09:53:22',0,'100 Neo Tiew Road, Singapore 719026',0,'\0',NULL,NULL,NULL,NULL,NULL,0,'1.4183191','103.71685309999998',1,'ChIJcfqTm-0N2jERGvgByW8S-PU','restaurant',1,0,'Bollywood Veggies','+65 6898 5001','bollywood+veggies','LOW','SA1','sa6','ACTIVE','http://bollywoodveggies.com/'),(3,'2016-01-19 09:09:22','2016-01-19 09:51:29',0,'10 Neo Tiew Lane 2, Singapore 718813',0,'\0',NULL,NULL,NULL,NULL,NULL,0,'1.4197759','103.71880179999994',1,'ChIJq8v8xvIN2jER2Jy4iL45AjI','lodging',1,0,'D\'Kranji Farm Resort','+65 6898 9228','d\'kranji+farm+resort','LOW','SA1','SA4','PROFILED','http://www.dkranji.com.sg/'),(4,'2016-01-19 09:09:49','2016-01-19 09:49:05',0,'11 Pasir Ris Street 41, Singapore 518934',0,'\0',NULL,NULL,NULL,NULL,NULL,0,'1.3728674','103.95947639999997',1,'ChIJWfHsY6092jERoJbkkNk2ECc','school',1,0,'Pasir Ris Crest Secondary School','+65 6581 1655','pasir+ris+crest+secondary+school','LOW','SA2','SA2','ACTIVE','http://www.prcss.moe.edu.sg/'),(5,'2016-01-19 09:09:50','2016-01-19 09:31:38',0,'1 Tampines Street 32, Singapore 529283',0,'\0',NULL,NULL,NULL,NULL,NULL,0,'1.3542893','103.95682009999996',1,'ChIJ4dRFNgM92jERpiiGM9jfBrM','school',1,0,'Ngee Ann Secondary School','+65 6784 4583','ngee+ann+secondary+school','LOW','SA2','SA2','PROSPECT','http://www.ngeeannsec.moe.edu.sg/'),(6,'2016-01-19 09:09:51','2016-01-19 09:30:56',0,'21 Tampines Street 45, Singapore 529093',0,'\0',NULL,NULL,NULL,NULL,NULL,0,'1.3610555','103.95592579999993',1,'ChIJVQFL2gA92jERBSa0CMNVVPA','school',1,0,'Dunman Secondary School','+65 6786 2668','dunman+secondary+school','LOW','SA2','SA3','PROSPECT','http://www.dunmansec.moe.edu.sg/'),(8,'2016-01-19 09:09:55',NULL,0,'971 Upper Changi Rd N, Singapore 507668',0,'\0',NULL,NULL,NULL,NULL,NULL,0,'1.360432','103.97046999999998',0,'ChIJ6R6ws_k82jERzDBmvxRfzGY','restaurant',0,0,'Porta Porta Restaurant','+65 6545 3108','porta+porta+restaurant','LOW','SA2',NULL,'SUSPECT','http://portaportaitalianrestaurant.com.sg/'),(9,'2016-01-19 09:10:02','2016-01-19 09:54:21',0,'1000 Upper Changi Rd N, Singapore 507707',0,'\0',NULL,NULL,NULL,NULL,NULL,0,'1.3621792','103.97401409999998',1,'ChIJCQHB6vc82jERq6iFty4Fzo4','museum',1,0,'The Changi Museum','+65 6214 2451','the+changi+museum','LOW','SA2','sa6','ACTIVE','http://www.changimuseum.sg/'),(10,'2016-01-19 09:10:08','2016-01-19 09:49:22',0,'3 Pasir Ris Drive 6, Singapore 519419',0,'\0',NULL,NULL,NULL,NULL,NULL,0,'1.3709326','103.95595000000003',1,'ChIJ28oExq092jERqI4U-Add9_k','school',1,0,'Loyang Primary School','+65 6582 1449','loyang+primary+school','LOW','SA2','SA3','PROPOSED','http://www.loyangpri.moe.edu.sg/'),(11,'2016-01-19 09:10:09',NULL,0,'2 Tampines Street 32, Singapore 529286',0,'\0',NULL,NULL,NULL,NULL,NULL,0,'1.3542861','103.96000690000005',0,'ChIJD2X0sQI92jERBUr_sJLGeAM','school',0,0,'Al-Amin Pre-school','+65 6786 0042','al-amin+pre-school','LOW','SA2',NULL,'SUSPECT',''),(12,'2016-01-19 09:10:33','2016-01-19 09:43:53',0,'321 Joo Chiat Pl, Singapore 427990',0,'\0',NULL,NULL,NULL,NULL,NULL,0,'1.315113','103.90914799999996',1,'ChIJEdOXOg8Y2jERcyuHlOlWbfY','hospital',1,0,'Parkway East Hospital','+65 6377 3737','parkway+east+hospital','LOW','SA3','SA5','PROPOSED','http://www.parkwayeast.com.sg/'),(13,'2016-01-19 09:10:35','2016-01-19 09:46:25',0,'238 Joo Chiat Rd, Singapore 427495',0,'\0',NULL,NULL,NULL,NULL,NULL,0,'1.3115639','103.90061230000003',1,'ChIJ37tThhEY2jERRBycMYB-rUA','lodging',1,0,'Venue Hotel The Lily','+65 6344 3131','venue+hotel+the+lily','LOW','SA3','SA2','PROFILED','http://www.venuehotel.sg/'),(14,'2016-01-19 09:10:35','2016-01-19 09:31:23',0,'219 Joo Chiat Road, Singapore 427485',0,'\0',NULL,NULL,NULL,NULL,NULL,0,'1.3115246','103.90130739999995',1,'ChIJH2EgPRIY2jERKrUAQ90dG9U','lodging',1,0,'The Fragrance Hotel','+65 6344 9888','the+fragrance+hotel','LOW','SA3','SA1','PROSPECT','http://www.fragrancehotel.com/'),(15,'2016-01-19 09:10:36','2016-01-19 09:32:04',0,'171 East Coast Rd, Singapore 428877',0,'\0',NULL,NULL,NULL,NULL,NULL,0,'1.3067805','103.90583560000005',1,'ChIJraA5MPwa2jERbXtj4LEfXiM','lodging',1,0,'Santa Grand Hotel East Coast','+65 6344 6866','santa+grand+hotel+east+coast','LOW','SA3','sa6','PROSPECT','http://www.santagrandhotels.com/'),(16,'2016-01-19 09:10:37','2016-01-19 09:33:46',0,'1 Onan Rd, Singapore 424780',0,'\0',NULL,NULL,NULL,NULL,NULL,0,'1.3155871','103.89735870000004',1,'ChIJrdRCtBAY2jERzuoRcMOJvxY','lodging',1,0,'Hotel 81 Tristar','+65 6244 8181','hotel+81+tristar','LOW','SA3','SA5','PROSPECT','http://www.hotel81.com.sg/'),(17,'2016-01-19 09:10:43','2016-01-19 09:14:48',0,'400 E Coast Rd, Singapore 428996',0,'\0',NULL,NULL,NULL,NULL,NULL,0,'1.3088209','103.91198129999998',1,'ChIJj7EB5woY2jER-lAGTbITwkk','restaurant',1,0,'Al Forno (East Coast) Pte Ltd','+65 6348 8781','al+forno+(east+coast)+pte+ltd','LOW','SA3',NULL,'PROSPECT','http://www.alfornoeastcoast.com.sg/'),(18,'2016-01-19 09:10:45',NULL,0,'11 Joo Chiat Pl, Singapore 427744',0,'\0',NULL,NULL,NULL,NULL,NULL,0,'1.3133239','103.90016270000001',1,'ChIJl8ByoBEY2jERliTRqHYPZrA','restaurant',1,0,'Chilli Padi the Nonya Family Restaurant','+65 6275 1002','chilli+padi+the+nonya+family+restaurant','LOW','SA3',NULL,'SUSPECT','http://www.chillipadi.com.sg/'),(19,'2016-01-19 09:10:47',NULL,0,'159 Joo Chiat Rd, Singapore 427436',0,'\0',NULL,NULL,NULL,NULL,NULL,0,'1.3127464','103.90015700000004',1,'ChIJqTY_gxEY2jERYmK8LIyivgo','restaurant',1,0,'Long Phung Vietnamese Restaurant','+65 9105 8519','long+phung+vietnamese+restaurant','LOW','SA3',NULL,'SUSPECT','http://www.longphungvietrest.com/'),(20,'2016-01-19 09:11:12','2016-01-19 09:32:15',0,'269 Jln Kayu, Singapore 799497',0,'\0',NULL,NULL,NULL,NULL,NULL,0,'1.3971205','103.87300619999996',1,'ChIJB8GqpX4W2jERrfqtMl1Xmr8','restaurant',1,0,'Spizza','+65 6377 7773','spizza','LOW','SA4','SA3','PROSPECT','http://www.spizza.sg/'),(21,'2016-01-19 09:11:13','2016-01-19 09:14:30',0,'277 Jln Kayu, Singapore 799505',0,'\0',NULL,NULL,NULL,NULL,NULL,0,'1.3973467','103.87302490000002',1,'ChIJEScmu34W2jERk-UcQmtiV2s','restaurant',1,0,'Jerry\'s Barbecue & Grill','+65 6484 0151','jerry\'s+barbecue+&+grill','LOW','SA4',NULL,'PROSPECT','http://www.jerrybbq.com/'),(22,'2016-01-19 09:11:15','2016-01-19 09:45:21',0,'21 Fernvale Link, Singapore 797702',0,'\0',NULL,NULL,NULL,NULL,NULL,0,'1.3906452','103.87846030000003',2,'ChIJhcXuImUW2jERs8y3Ohdg0s8','school',2,0,'Pei Hwa Secondary School','+65 6500 9580','pei+hwa+secondary+school','LOW','SA4','SA1','ACTIVE','http://www.peihwasec.moe.edu.sg/'),(23,'2016-01-19 09:11:17',NULL,0,'258 Jln Kayu, Singapore 799487',0,'\0',NULL,NULL,NULL,NULL,NULL,0,'1.3982126','103.87324280000007',1,'ChIJddKNzn4W2jERVGrDRcd5OxY','restaurant',1,0,'Thohirah Restaurant','+65 6481 2009','thohirah+restaurant','LOW','SA4',NULL,'SUSPECT',''),(24,'2016-01-19 09:11:19',NULL,0,'239 Jln Kayu, Singapore 799463',0,'\0',NULL,NULL,NULL,NULL,NULL,0,'1.3963235','103.87293210000007',1,'ChIJCzL6GnwW2jERtpXp_2IKVbI','restaurant',1,0,'Thasevi Food','+65 6481 1537','thasevi+food','LOW','SA4',NULL,'SUSPECT',''),(25,'2016-01-19 09:11:45','2016-01-19 09:33:56',0,'30 Jurong West Street 61, Singapore 648368',0,'\0',NULL,NULL,NULL,NULL,NULL,0,'1.3391899','103.69895059999999',1,'ChIJkR1ecZEP2jERJhLeZx32rPs','school',1,0,'Jurong West Primary School','+65 6793 3419','jurong+west+primary+school','LOW','SA5','SA4','PROSPECT','http://www.jurongwestpri.moe.edu.sg/'),(26,'2016-01-19 09:11:45','2016-01-19 09:45:31',0,'510 Upper Jurong Rd, Singapore 638365',0,'\0',NULL,NULL,NULL,NULL,NULL,0,'1.3326105','103.67911049999998',1,'ChIJq7P-2n4P2jERoflGFhUAEfQ','museum',1,0,'S\'pore Discovery Centre','+65 6792 6188','s\'pore+discovery+centre','LOW','SA5','SA1','PROFILED','http://www.sdc.com.sg/'),(28,'2016-01-19 09:11:48','2016-01-19 09:30:35',0,'50 Nanyang Ave, Singapore 639798',0,'\0',NULL,NULL,NULL,NULL,NULL,0,'1.3483099','103.68313469999998',2,'ChIJY0QBmQoP2jERGYItxQAIu7g','university',2,0,'Nanyang Technological University','+65 6791 1744','nanyang+technological+university','LOW','SA5','SA4','PROSPECT','http://www.ntu.edu.sg/Pages/index.aspx'),(30,'2016-01-19 09:11:50','2016-01-19 09:13:42',0,'5 Jurong West Street 91, Singapore 649036',0,'\0',NULL,NULL,NULL,NULL,NULL,0,'1.3429078','103.68766740000001',1,'ChIJ0c1cxJ4P2jERiZ70xxE_V1I','school',1,0,'Xingnan Primary School','+65 6791 3679','xingnan+primary+school','LOW','SA5',NULL,'PROSPECT','http://www.xingnanpri.moe.edu.sg/'),(32,'2016-01-19 09:12:00',NULL,0,'12 Nanyang Dr, Singapore 637721',0,'\0',NULL,NULL,NULL,NULL,NULL,0,'1.34388','103.68405000000007',1,'ChIJwxpUtZ8P2jERPbeGV3upuaY','museum',1,0,'Chinese Heritage Centre','+65 6790 6176','chinese+heritage+centre','LOW','SA5',NULL,'SUSPECT','http://chc.ntu.edu.sg/'),(33,'2016-01-19 09:12:01',NULL,0,'23 Jurong West Street 81, Singapore 649076',0,'\0',NULL,NULL,NULL,NULL,NULL,0,'1.348486','103.69460900000001',1,'ChIJhy8117wP2jERR780-PDeTpU','school',1,0,'Pioneer Primary School','+65 6793 2397','pioneer+primary+school','LOW','SA5',NULL,'SUSPECT','http://www.pioneerpri.moe.edu.sg/'),(40,'2016-01-19 09:12:59',NULL,0,'Choa Chu Kang Ave 4, Singapore 689812',0,'\0',NULL,NULL,NULL,NULL,NULL,0,'1.385326','103.74507010000002',0,'ChIJcVbOmekR2jERRaQoJI8-QDM','spa',0,0,'Jean Yip Spa','+65 6764 9615','jean+yip+spa','LOW','sa6',NULL,'SUSPECT','http://jeanyipgroup.com/'),(45,'2016-01-19 09:28:55','2016-01-19 10:47:49',0,'432 synthesis\r\nFuisionopolis walk\r\nsingapore',50000,'\0','mr. singh','85789577','Owner',NULL,'singh@gmail.com',50,NULL,NULL,4,NULL,'bar',5,0,'Mac\'s Cafe and bar','6443 0658','mac\'s+cafe+and+bar','MEDIUM','Website','SA5','ACTIVE','http://www.homestolife.com/'),(46,'2016-01-19 09:56:43','2016-01-19 10:42:53',0,'1 north road\r\nsignapore',14000,'\0','Mr. Paolo','85789654','Manager',NULL,'paolo@gmail.com',40,NULL,NULL,3,NULL,'restaurant',4,0,'Pietrasanta The Italian Restaurant','85749874','pietrasanta+the+italian+restaurant','MEDIUM','Manager','SA1','PROSPECT','http://www.homestolife.com/');
/*!40000 ALTER TABLE `client` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-19 11:53:04
