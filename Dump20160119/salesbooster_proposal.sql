-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: salesbooster
-- ------------------------------------------------------
-- Server version	5.7.9-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `proposal`
--

DROP TABLE IF EXISTS `proposal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proposal` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `version` bigint(20) NOT NULL,
  `clientid` bigint(20) NOT NULL,
  `closed` bit(1) NOT NULL,
  `cost` float NOT NULL,
  `discount` float NOT NULL,
  `discountedprice` float NOT NULL,
  `end` date DEFAULT NULL,
  `expired` bit(1) NOT NULL,
  `givendiscount` float NOT NULL,
  `loyaltydis` float NOT NULL,
  `numberofcleaners` bigint(20) NOT NULL,
  `perdaycost` float NOT NULL,
  `price` float NOT NULL,
  `profit` float NOT NULL,
  `salesrep` varchar(255) DEFAULT NULL,
  `start` date DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `validfor` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proposal`
--

LOCK TABLES `proposal` WRITE;
/*!40000 ALTER TABLE `proposal` DISABLE KEYS */;
INSERT INTO `proposal` VALUES (1,'2016-01-19 09:39:43','2016-01-19 09:40:35',0,1,'',2048,0.05,3112.2,'2016-05-17','\0',0.05,0,1,76,3276,1064.2,'SA5','2016-01-26','WON',16),(2,'2016-01-19 09:42:45','2016-01-19 09:43:42',0,12,'',15556.8,0.08,22898.8,'2016-04-12','\0',0.08,0,2,185.2,24890,7342,'SA5','2016-01-19','LOST',12),(3,'2016-01-19 09:45:05','2016-01-19 09:45:21',0,22,'',624,0,998,'2016-04-23','\0',0,0,1,52,998,374,'SA1','2016-01-30','WON',12),(4,'2016-01-19 09:48:26','2016-01-19 09:49:05',0,4,'',1730,0.05,2629.6,'2016-03-17','\0',0.05,0,1,106.25,2768,899.6,'SA2','2016-01-21','WON',8),(5,'2016-01-19 09:50:13','2016-01-19 09:51:09',0,10,'\0',1976,0.08,2908.12,'2016-03-19','\0',0.08,0,1,67,3161,932.12,'SA3','2016-01-23','PROPOSED',8),(6,'2016-01-19 09:53:17','2016-01-19 09:53:22',0,2,'',1248,0,1996,'2016-03-17','\0',0,0,1,52,1996,748,'sa6','2016-01-21','WON',8),(7,'2016-01-19 09:53:59','2016-01-19 09:54:21',0,9,'',192,0.2,245.6,'2016-03-27','\0',0.2,0,1,24,307,53.6,'sa6','2016-01-31','WON',8),(9,'2016-01-19 10:46:40','2016-01-19 10:47:49',0,45,'',2626,0.2,3360.8,'2016-07-26','\0',0.2,0,1,50.5,4201,734.8,'SA5','2016-01-26','WON',26),(10,'2016-01-19 10:49:58',NULL,0,1,'\0',3328,0,5324,'2016-11-15','\0',0,0,1,76,5324,1996,'SA5','2016-05-17','OPEN',26);
/*!40000 ALTER TABLE `proposal` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-19 11:53:04
