$(document).ready(
		function()
		{
			var table = $('.datatable').DataTable(
			{
				"jQueryUI" : false,
				"lengthChange" : true,
				"lengthMenu" : [ 10, 25, 50, 75, 100 ],
				"columnDefs" : [
				{
					"targets" : 'nosort',
					"orderable" : false
				},
				{
					"targets" : 'nosearch',
					"searchable" : false
				},
				{
					"targets" : 'noshow',
					"visible" : false
				},

				{
					"targets" : 'cannull',
					"defaultContent" : "<i>Not set</i>"
				},
				// {
				// orderable : false,
				// className : 'select-checkbox',
				// targets : 0
				// },
				// {
				// "className" : 'details-control',
				// "orderable" : false,
				// "width" : "2%",
				// "data" : null,
				//				
				// "defaultContent" : '',
				// "targets" : 0
				// },
				// {
				// "data" : null,
				// "defaultContent" : "<button>Edit</button>",
				// "targets" : -1
				// }
				],

				"initComplete" : function(settings, json)
				{

				},
				"rowCallback" : function(row, data, index)
				{

				},
				// "dom" : 'Bfrtip',
				// "buttons" : [
				// {
				// text : 'Get selected data',
				// action : function()
				// {
				// var count = table.rows(
				// {
				// selected : true
				// }).count();
				//						
				// alert(count);
				// }
				// } ],

				"renderer" : "bootstrap",
				"responsive" :
				{
					"details" :
					{
						display : $.fn.dataTable.Responsive.display.modal(
						{
							header : function(row)
							{
								var data = row.data();
								return 'Details for ' + data[0] + ' ' + data[1];
							}
						}),
						renderer : function(api, rowIdx, columns)
						{
							var data = $.map(columns, function(col, i)
							{
								return '<tr>' + '<td>' + col.title + ':' + '</td> ' + '<td>' + col.data + '</td>' + '</tr>';
							}).join('');

							return $('<table class="table"/>').append(data);
						}
					}
				},
				"scrollX" : false,
			// "select" :
			// {
			// style : 'multi',
			// selector : 'td:first-child'
			// }

			});

			/* Formatting function for row details - modify as you need */
			function format(d)
			{

				console.log(d);

				// `d` is the original data object for the row
				return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' + '<tr>' + '<td>Full name:</td>' + '<td>'
						+ d[2] + '</td>' + '</tr>' + '<tr>' + '<td>Extension number:</td>' + '<td>' + '</td>' + '</tr>' + '<tr>'
						+ '<td>Extra info:</td>' + '<td>And any further details here (images etc)...</td>' + '</tr>' + '</table>';
			}

			table.on('click', '.quick-details', function()
			{
				var tr = $(this).closest('tr');
				var row = table.row(tr);

				var icon = $(this).children('i');

				if (row.child.isShown())
				{

					icon.removeClass('fa-eye-slash').addClass('fa-eye');

					// This row is already open - close it
					row.child.hide();
					tr.removeClass('shown');
				} else
				{

					icon.removeClass('fa-eye').addClass('fa-eye-slash');

					// Open this row
					row.child(format(row.data())).show();
					tr.addClass('shown');
				}
			});

			table.on('select', function(e, dt, type, indexes)
			{

				var row = table.row(indexes);

				// // // Open this row
				// row.child(format(row.data())).show();
				// $(e.target).addClass('shown');

			}).on('deselect', function(e, dt, type, indexes)
			{

				var row = table.row(indexes);

				//				// // // This row is already open - close it
				//				row.child.hide();
				//				$(e.target).removeClass('shown');

			});

		});