/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {

	
	config.extraPlugins = 'uploadimage';
	config.uploadUrl = '/sb/ckeditorUplaod';
	config.height = 500;
	config.autosave_NotOlderThen = 14400;
	config.autosave_saveOnDestroy = true;
};
