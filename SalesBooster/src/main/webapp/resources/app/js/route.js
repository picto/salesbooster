var directionsService;
var directionsRenderer;
var map;

function initialize() {
	var position = new google.maps.LatLng(-8.05,-34.89);
	
	directionsService = new google.maps.DirectionsService();
	directionsRenderer = new google.maps.DirectionsRenderer();
	map = new google.maps.Map(document.getElementById("map"), {
		zoom: 16,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		center: position
	});
	
	directionsRenderer.setMap(map);
	
	google.maps.event.addListener(map, 'click', function(event) {
		addWayPointToRoute(event.latLng);
    });
}

var markers = [];
var polylines = [];
var isFirst = true;

function addWayPointToRoute(location) {
	if (isFirst) {
		addFirstWayPoint(location);
		isFirst = false;
	} else {
		appendWayPoint(location);
	}
}

function addFirstWayPoint(location) {
	var request = {
		origin: location,
		destination: location,
		travelMode: google.maps.DirectionsTravelMode.DRIVING
	};
	directionsService.route(request, function(response, status) {
		if (status == google.maps.DirectionsStatus.OK) {
			var marker = new google.maps.Marker({
				position: response.routes[0].legs[0].start_location, 
				map: map,
				draggable : true
			});
				marker.arrayIndex = 0;
				markers.push(marker);
				google.maps.event.addListener(marker, 'dragend', function() {
				recalculateRoute(marker);
			});
		}
	});
}

function appendWayPoint(location) {
	var request = {
		origin: markers[markers.length - 1].position,
		destination: location,
		travelMode: google.maps.DirectionsTravelMode.DRIVING
	};
	
	directionsService.route(request, function(response, status) {
		if (status == google.maps.DirectionsStatus.OK) {
			var marker = new google.maps.Marker({
				position: response.routes[0].legs[0].end_location, 
				map: map,
				draggable : true
			});
			markers.push(marker);
			marker.arrayIndex = markers.length - 1;
			google.maps.event.addListener(marker, 'dragend', function() {
				recalculateRoute(marker);
			});
			
			var polyline = new google.maps.Polyline();
			var path = response.routes[0].overview_path;
			for (var x in path) {
				polyline.getPath().push(path[x]);
			}
			polyline.setMap(map);
			polylines.push(polyline);
		}
	});
}

function recalculateRoute(marker) { //recalculate the polyline to fit the new position of the dragged marker
	if (marker.arrayIndex > 0) { //its not the first so recalculate the route from previous to this marker
		polylines[marker.arrayIndex - 1].setMap(null);
		
		var request = {
			origin: markers[marker.arrayIndex - 1].position,
			destination: marker.position,
			travelMode: google.maps.DirectionsTravelMode.DRIVING
		};
		
		directionsService.route(request, function(response, status) {
			if (status == google.maps.DirectionsStatus.OK) {
				var polyline = new google.maps.Polyline();
				var path = response.routes[0].overview_path;
				for (var x in path) {
					polyline.getPath().push(path[x]);
				}
				polyline.setMap(map);
				polylines[marker.arrayIndex - 1] = polyline;
			}
		});
	}
	if (marker.arrayIndex < markers.length - 1) { //its not the last, so recalculate the route from this to next marker
		polylines[marker.arrayIndex].setMap(null);
		
		var request = {
			origin: marker.position,
			destination: markers[marker.arrayIndex + 1].position,
			travelMode: google.maps.DirectionsTravelMode.DRIVING
		};
		
		directionsService.route(request, function(response, status) {
			if (status == google.maps.DirectionsStatus.OK) {
				var polyline = new google.maps.Polyline();
				var path = response.routes[0].overview_path;
				for (var x in path) {
					polyline.getPath().push(path[x]);
				}
				polyline.setMap(map);
				polylines[marker.arrayIndex] = polyline;
			}
		});
	}
}

function placeMarker(location) {
	var request = {
		origin: location, 
		destination: location,
		travelMode: google.maps.DirectionsTravelMode.DRIVING
	};
	directionsService.route(request, function(response, status) {
		if (status == google.maps.DirectionsStatus.OK) {
			var marker = new google.maps.Marker({
				position: response.routes[0].legs[0].start_location, 
				map: map
			});
		}
	});
}