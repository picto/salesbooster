function addData(data)
{

	for ( var index in data)
	{

		var place = data[index];

		var elemId = place.name.toLowerCase().replace(/ /g, '+');

		var detailStr = "";
		detailStr += "<button class=\"btn btn-raised btn-xs details\" title=\" Details\"><i class=\"fa fa-eye fa-lg\"><\/i></a></button>";

		detailStr += '<button class="btn btn-xs btn-raised sd-add" ><i class="fa fa-plus-square fa-lg"><div class="data" style="display:none;" >'
				+ dataObj + '</div></i></button>';

		detailStr += '<button class="delete-place btn btn-xs btn-raised sd-remove"><i class="fa fa-trash fa-lg"></i></button>';

		var gSearchUrl = getGoogleSearchUrl(place.name, '');

		var actionsStr = '';
		actionsStr += '<button class="btn btn-xs btn-raised"><a href="' + gSearchUrl
				+ '" target=_blank><i class="fa fa-google fa-lg"></i></a></button>';

		if (isset(place.website))
		{
			actionsStr += '<button class="btn btn-xs btn-raised" ><a href="' + place.website
					+ '" target=_blank><i class="fa fa-map-marker fa-lg"></i></a></button>';
		}

		var dataObj =
		{
			'' : '',
			'name' : place.name,
			'address' : place.address,
			'phone' : isset(place.phone) ? place.phone : "",
			'website' : isset(place.website) ? place.website : "",
			'placeid' : elemId,
			'industry' : place.types,
			'details' : detailStr,
			'actions' : actionsStr,
			'time' : new Date().getTime()
		}

		if (placesTable.rows('[id="' + dataObj.placeid + '"]').any())
		{
			console.log('already exist cannot be added');
			notify("Already Exists!!", "Business " + dataObj.name + " alredy in the list .", 'warning', 500);
		} else
		{
			placesTable.row.add(dataObj).draw();
			notify("Found New!!", "Please Scroll Down for " + dataObj.name + " .", 'info', 500);
		}

	}
}

function formSubmit()
{
	$('#url-form').submit(function(event)
	{

		var form = $(this);

		$.ajax(
		{
			type : form.attr('method'),
			url : form.attr('action'),
			data : form.serialize()
		}).done(function(data)
		{
			// Optionally alert the user of success here...

			addData(data);
			console.log(data);
		}).fail(function(data)
		{
			// Optionally alert the user of an error here...

			console.log(data);
		});
		event.preventDefault(); // Prevent the form from submitting via the
		// browser.

	});
}

var page = 1;
var urlMain;

function moreClicked()
{

	var url = document.getElementById('url-input').value;

	if (!isset(urlMain) || empty(urlMain))
	{

		var paths = url.split('/');

		paths.pop();
		if (is_numeric(paths[paths.length - 1]))
		{

			page = paths[paths.length - 1];

			paths.pop();
			paths.pop();

		}

		urlMain = paths.join('/');
	}

	if (!empty(urlMain))
	{
		page++;
		url = urlMain + '/All/' + page + '/';
	}

	console.log(url);
	console.log(urlMain);

	document.getElementById('url-input').value = url;

	$('#url-form').submit();

	if (page == 10)
		document.getElementById("more").disabled = true;

}

var placesTable;

$(document).ready(function()
{

	placesTable = $('.sd-table').DataTable(
	{
		"jQueryUI" : false,
		"lengthChange" : false,
		"autoWidth" : false,
		"order" : [ [ 9, 'asc' ] ],
		"scrollY" : "550px",
		"scrollCollapse" : true,
		"paging" : false,
		"columnDefs" : [
		{
			"targets" : 'nosort',
			"orderable" : false
		},
		{
			"targets" : 'nosearch',
			"searchable" : false
		},
		{
			"targets" : 'noshow',
			"visible" : false
		},

		{
			"targets" : 'cannull',
			"defaultContent" : "<i>Not set</i>"
		},
		{
			orderable : false,
			className : 'select-checkbox',
			targets : 0
		}, ],

		"initComplete" : function(settings, json)
		{

		},
		"rowCallback" : function(row, data, index)
		{

		},
		"dom" : 'irtB',
		"buttons" : [
		{
			extend : 'selectAll',
			className : 'btn-dt'
		},
		{
			extend : 'selectNone',
			className : 'btn-dt'
		},
		{
			text : 'Add Selected',
			className : 'btn-dt',
			action : function()
			{
				var selectedRows = placesTable.rows(
				{
					selected : true
				}).data();

				selectedRows.each(function(row)
				{

					console.log(row);

					ajaxJsonPost(singleSuspectSaveUrl, row, function(data)
					{
						console.log(data);
						if (data.success)
						{
							showStatus(true, "New Suspect " + row.name + " added to the Database")
						} else
						{
							showStatus(false, "Client " + row.name + " already exists. So removed from the list");
						}

						placesTable.row('[id="' + row.placeid + '"]').remove().draw();
					}, function(xhr)
					{
						console.log(xhr);
					});

				});

			}
		},
		{
			text : 'Remove Selected',
			className : 'btn-dt',
			action : function()
			{
				placesTable.rows(
				{
					selected : true
				}).remove().draw();

				notify("Entries Removed", 'Selected entries removed', 'warning', 1000);
			}
		}, ],
		"columns" : [
		{
			"data" : ""
		},
		{
			"data" : "name"
		},
		{
			"data" : "address"
		},
		{
			"data" : "phone"
		},
		{
			"data" : "website"
		},
		{
			"data" : "placeid"
		},
		{
			"data" : "industry"
		},
		{
			"data" : "details"
		},
		{
			"data" : "actions"
		},
		{
			"data" : "time"
		},

		],

		"rowId" : "placeid",
		"renderer" : "bootstrap",
		"responsive" :
		{
			"details" :
			{
				display : $.fn.dataTable.Responsive.display.modal(
				{
					header : function(row)
					{
						var data = row.data();
						return 'Details for ' + data[0] + ' ' + data[1];
					}
				}),
				renderer : function(api, rowIdx, columns)
				{
					var data = $.map(columns, function(col, i)
					{
						return '<tr>' + '<td>' + col.title + ':' + '</td> ' + '<td>' + col.data + '</td>' + '</tr>';
					}).join('');

					return $('<table class="table"/>').append(data);
				}
			}
		},
		"select" :
		{
			style : 'multi',
			selector : 'td:first-child'
		}

	});

	/* Formatting function for row details - modify as you need */
	function format(d)
	{

		console.log(d);

		var strVar = "";
		strVar += "<div class=\"card\">";
		strVar += "		<h4 class=\"page-header\" >Details<\/h4>";
		strVar += "							<table class=\"table table-bordered\">";
		strVar += "								<tbody>";
		strVar += " <tr>";
		strVar += " <td><label>Phone<\/label><\/td>";
		strVar += " <td><label>" + d.phone + "<\/label><\/td>";
		strVar += " <\/tr> ";
		strVar += " <tr>";
		strVar += " <td><label>Website<\/label><\/td>";
		strVar += " <td><label>" + d.website + "<\/label><\/td>";
		strVar += " <\/tr> ";
		strVar += "	<tr>";
		strVar += " <td><label>Types<\/label><\/td>";
		strVar += " <td><label>" + d.industry + "<\/label><\/td>";
		strVar += "	<\/tr>								";
		strVar += "	<tr>";
		strVar += " <td><label>Actions<\/label><\/td>";
		strVar += " <td><label>" + d.actions + "<\/label><\/td>";
		strVar += "	<\/tr>								";
		strVar += "	<\/tbody>";
		strVar += "	<\/table>";
		strVar += "	<\/div>";

		return strVar;

	}

	placesTable.on('click', '.details', function()
	{
		var tr = $(this).closest('tr');
		var row = placesTable.row(tr);

		var icon = $(this).children('i');

		if (row.child.isShown())
		{
			icon.removeClass('fa-eye-slash').addClass('fa-eye');

			// This row is already open - close it
			row.child.hide();
			tr.removeClass('shown');

		} else
		{

			icon.removeClass('fa-eye').addClass('fa-eye-slash');

			// Open this row
			row.child(format(row.data())).show();
			tr.addClass('shown');
		}
	});

	placesTable.on('click', '.sd-add', function()
	{
		var tr = $(this).closest('tr');
		var row = placesTable.row(tr);

		var rowData = row.data();

		ajaxJsonPost(singleSuspectSaveUrl, rowData, function(data)
		{
			console.log(data);

			if (data.success)
			{

				showStatus(true, "New Suspect added to the Database")
				row.remove().draw();

			} else
			{

				showStatus(false, "Client already exists. So removed from the list");
				row.remove().draw();
			}

		}, function(xhr)
		{
			console.log(xhr);
		});

	});
	placesTable.on('click', '.sd-remove', function()
	{
		var tr = $(this).closest('tr');
		var row = placesTable.row(tr);

		row.remove().draw();

		notify('Removed!!', 'Entry removed', 'warning', 1000);

	});

});
