/**
 * 
 */
''

$(function()
{

	var reqInfotable = $('.req-date-table').DataTable(
	{
		"jQueryUI" : false,
		"autoWidth" : false,
		"lengthChange" : true,
		"order" : [ [ 1, 'desc' ] ],
		"columnDefs" : [
		{
			"targets" : 'nosort',
			"orderable" : false
		},
		{
			"targets" : 'nosearch',
			"searchable" : false
		},
		{
			"targets" : 'noshow',
			"visible" : false
		},

		{
			"targets" : 'cannull',
			"defaultContent" : "<i>Not set</i>"
		} ],

		"initComplete" : function(settings, json)
		{

		},
		"rowCallback" : function(row, data, index)
		{

		},
		"dom" : 'Brtp',
		"buttons" : [
		{
			text : 'Add New',
			className : "btn-dt",
			action : function()
			{

                bootbox.dialog({

                    title:"Add Requiremnt Informations",
                    message: $(".add-req-date-modal").html(),
                    onEscape:true,

                });





                $('.bootbox-body .req-date-form').submit(function(e)
                {

                    var url = $(this).attr('action');
                    var values =
                    {};
                    $.each($(this).serializeArray(), function(i, field)
                    {
                        values[field.name] = field.value;
                    });

                    console.log(values);

                    ajaxJsonPost(url, values, function(data)
                    {



						showStatus(data.success,data.msg);






						if(data.success)
						{


							var obj =
							{
								0: data.data.id,
								1: data.data.start.values[0]+'-'+ data.data.start.values[1]+'-'+ data.data.start.values[2],

								2: data.data.weeks,
								3: '<a class=\"btn btn-raised btn-xs delete-info\" title=\"Delete Info\">Delete <\/a>'
							}


							console.log(data.data.start)

							reqInfotable.row.add(obj).draw();
						}




                    }, function()
                    {
                    });

                    bootbox.hideAll();
                    e.preventDefault();
                });








            }
		} ],

		"renderer" : "bootstrap",

		"scrollX" : false,

	});


	reqInfotable.on('click', '.delete-info', function()
	{
		var tr = $(this).closest('tr');
		var row = reqInfotable.row(tr);

		var rowData = row.data();

		var url = deleteReqInfo+'/'+rowData[0];


		ajaxJsonGET(url,{},function(data){



			showStatus(data.success,data.msg);

			if(data.success)
			{
				row.remove().draw();
			}



		},function(){});


	});

    var reqTable = $('.req-table').DataTable(
	{
		"jQueryUI" : false,
		"autoWidth" : false,
		"lengthChange" : true,
		"lengthMenu" : [ 10, 15, 25, 50, 75, 100 ],
		"order" : [ [ 1, 'asc' ], [ 2, 'asc' ], ],
		"columnDefs" : [
		{
			"targets" : 'nosort',
			"orderable" : false
		},
		{
			"targets" : 'nosearch',
			"searchable" : false
		},
		{
			"targets" : 'noshow',
			"visible" : false
		},

		{
			"targets" : 'cannull',
			"defaultContent" : "<i>Not set</i>"
		} ],

		"initComplete" : function(settings, json)
		{

		},
		"rowCallback" : function(row, data, index)
		{

		},
		"dom" : 'Bfrtp',
		"buttons" : [
		{
			text : 'Add Requirement',
			className : "btn-dt",
			action : function()
			{

				$(location).attr('href', reqUrl);

			}
		} ],

		"renderer" : "bootstrap",

		"scrollX" : false,

	});

	function format(d)
	{


		console.log(d[3]);


		var dayStr = d[7]? " Day":"";
		dayStr += d[8]? " Night":"";

		var strVar = "";
		strVar += "<div class=\"card\">";
		strVar += "		<h4 class=\"page-header\" >Quick Details<\/h4>";
		strVar += "							<table class=\"table\">";
		strVar += "								<tbody>";
		strVar += "									<tr>";
		strVar += "										<td><label>Days in weeks<\/label><\/td>";
		strVar += "										<td><label>" + d[3] + "<\/label><\/td>";
		strVar += "									<\/tr>								";
		strVar += "									<tr>";
		strVar += "										<td><label>Times in a Shift<\/label><\/td>";
		strVar += "										<td><label>" + d[4] + "<\/label><\/td>";
		strVar += "									<\/tr>								";
		strVar += "									<tr>";
		strVar += "										<td><label>Area to clean in Sq m.<\/label><\/td>";
		strVar += "										<td><label>" + d[5] + "<\/label><\/td>";
		strVar += "									<\/tr>							";
		strVar += "									<tr>";
		strVar += "										<td><label>Shift<\/label><\/td>";
		strVar += "										<td><label>" + dayStr + "<\/label><\/td>";
		strVar += "									<\/tr>							";
		strVar += "									<tr>";
		strVar += "										<td><label>Required by client<\/label><\/td>";
		strVar += "										<td><label>" + d[8] + "<\/label><\/td>";
		strVar += "									<\/tr>							";
		strVar += "								<\/tbody>";
		strVar += "							<\/table>";
		strVar += "	<\/div>";

		return strVar;

	}

	reqTable.on('click', '.quick-details', function()
	{
		var tr = $(this).closest('tr');
		var row = reqTable.row(tr);

		var icon = $(this).children('i');

		if (row.child.isShown())
		{

			icon.removeClass('fa-eye-slash').addClass('fa-eye');

			// This row is already open - close it
			row.child.hide();
			tr.removeClass('shown');
		} else
		{

			icon.removeClass('fa-eye').addClass('fa-eye-slash');

			// Open this row
			row.child(format(row.data())).show();
			tr.addClass('shown');
		}
	});



});