function sleep(delay)
{
	var start = new Date().getTime();
	while (new Date().getTime() < start + delay)
		;
}

var directionsService;
var directionsRenderer;
var distanceService;
var map;

var waypoints;
var renderers = [];

var dist = [];
var best;
var polylines = [];
var markers = [];

var loopCount;

function createMarker(waypoint)
{

	var marker = new google.maps.Marker(
	{
		position : waypoint,
		map : map
	});
	markers.push(marker);
}

function initialize()
{

	var position = new google.maps.LatLng(-8.05, -34.89);

	waypoints = [ new google.maps.LatLng(-8.04745931112481, -34.896676540374756), new google.maps.LatLng(-8.048372903858715, -34.89927291870117),
			new google.maps.LatLng(-8.046248266419221, -34.90176200866699), new google.maps.LatLng(-8.052728375778878, -34.9010968208313) ];

	directionsService = new google.maps.DirectionsService();
	directionsRenderer = new google.maps.DirectionsRenderer();
	map = new google.maps.Map(document.getElementById("map"),
	{
		zoom : 16,
		mapTypeId : google.maps.MapTypeId.ROADMAP,
		center : position
	});

	directionsRenderer.setMap(map);

	for ( var i in waypoints)
	{
		createMarker(waypoints[i]);
	}

	google.maps.event.addListener(map, 'click', function(event)
	{

		newwp = event.latLng;
		createMarker(newwp);
		waypoints.push(newwp);
		distanceService = new google.maps.DistanceMatrixService

		var options =
		{
			origins : waypoints,
			destinations : waypoints,
			travelMode : google.maps.TravelMode.DRIVING,
		}

		distanceService.getDistanceMatrix(options, function(response, status)
		{
			if (status !== google.maps.DistanceMatrixStatus.OK)
			{
				alert('Error was: ' + status);

			} else
			{

				var originList = response.originAddresses;

				var indexTrack = Object.keys(originList);

				newwp = waypoints.pop();
				var newIndex = indexTrack.pop();

				for (var i = 0; i < waypoints.length; i++)
				{
					dist[i] = 0;
					waypoints.splice(i, 0, newwp);
					indexTrack.splice(i, 0, newIndex);
					for (var j = 0; j < waypoints.length - 1; j++)
					{
						dist[i] += response.rows[indexTrack[j]].elements[indexTrack[j + 1]].distance.value;
					}
					console.log(indexTrack);
					console.log(dist[i]);

					var listElem = document.getElementById('list');

					listElem.innerHTML += '<li><h3>' + dist[i] + '</h3><p>' + JSON.stringify(indexTrack) + '</p></li>';

					console.log(listElem.innerHTML);

					newIndex = indexTrack[i];
					indexTrack.splice(i, 1);
					newwp = waypoints[i];
					waypoints.splice(i, 1);
				}

				console.log(dist);

			}

		});

	});
}
