/**
 * Created by mamun on 10/1/2016.
 */


$(document).ready(function(){


    function getSparklineData(dataObj)
    {

        var dataArr = [];

        for(var  i in dataObj)
        {
            dataArr.push(dataObj[i].revenue);
        }

        return dataArr;
    }



    function getCurrentSalesStats()
    {

        ajaxJsonGET(getCurrentSalesStatsUrl,{},
            function(data){

                $(".revenue-txt").text(formatNumberToMoney(data.sales.revenue,""));
                $(".opp-txt").text(formatNumberToMoney(data.sales.opportunity,""));
                $(".target-txt").text(formatNumberToMoney(data.sales.target,""));
                $(".profit-txt").text(formatNumberToMoney(data.sales.profit,""));


                var won = data.deals.won;
                var wonArr = getSparklineData(won)

                var lost = data.deals.lost;
                var lostArr = getSparklineData(lost)

                var current = data.deals.current;
                var curArr = getSparklineData(current)

                var latest = data.deals.latest;
                var latestArr = getSparklineData(latest)



            SparkLineDrawBarGraph($('#sparkline-won'), wonArr,'#B25050' );
            SparkLineDrawBarGraph($('#sparkline-current'), curArr, '#7BC5D3');
            SparkLineDrawBarGraph($('#sparkline-lost'), lostArr, "#BF3F3F");
            SparkLineDrawBarGraph($('#sparkline-latest'), latestArr, '#B25050');

                console.log(latestArr);




            }, function (xhr) {

                console.log(data);

                alert(data);


            }
        );
    }

    getCurrentSalesStats();




    setInterval(function(){



        getCurrentSalesStats();


    },3000);


});

