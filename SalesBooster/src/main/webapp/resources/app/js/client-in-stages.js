/**
 * Created by mamun on 10/1/2016.
 */


$(document).ready(function(){


    function getMorrisData(dataObj)
    {

        var morrisData = [];

        var countArr = {};




        for(var i in dataObj)
        {
            if(!isset(countArr[dataObj[i].status]))
            {
                countArr[dataObj[i].status] = 1;
            }else
                countArr[dataObj[i].status] += 1;

        }


        for(var i in countArr)
        {
            morrisData.push({'x':i,'y':countArr[i]});
        }


        return morrisData;
    }



    function getClientInStages()
    {

        ajaxJsonGET(getClientInStagesUrl,{},
            function(data){





            // Use Morris.Area instead of Morris.Line
            Morris.Bar(
                {
                    element : 'clients',
                    data : getMorrisData(data.data),
                    xkey : 'x',
                    ykeys :  ['y'] ,
                    labels: ['# of Clients'],

                }).on('click', function(i, row)
            {
                console.log(i, row);
            });




            }, function (xhr) {

                console.log(data);

                alert(data);


            }
        );
    }

    getClientInStages();




    setInterval(function(){



        //getClientInStages();


    },300000);


});

