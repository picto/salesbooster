/**
 *
 */
''

$(function () {

    var propTable = $('.prop-table').DataTable(
        {
            "jQueryUI": false,
            "autoWidth": false,
            "lengthChange": true,
            "lengthMenu": [10, 15, 25, 50, 75, 100],
            "order": [[0, 'desc']],
            "columnDefs": [
                {
                    "targets": 'nosort',
                    "orderable": false
                },
                {
                    "targets": 'nosearch',
                    "searchable": false
                },
                {
                    "targets": 'noshow',
                    "visible": false
                },

                {
                    "targets": 'cannull',
                    "defaultContent": "<i>Not set</i>"
                }],

            "initComplete": function (settings, json) {

            },
            "rowCallback": function (row, data, index) {

            },
            "dom": 'Bfrtp',
            "buttons": [
                {
                    text: 'New Proposal',
                    className: "btn-dt",
                    action: function () {

                        var createNewProposal = function () {
                            ajaxJsonGET(newProposalUrl,
                                {}, function (data) {

                                    //console.log(data);

                                    showStatus(data.success, data.msg);


                                    if(data.success)
                                    {
                                        var obj = data.data;

                                        console.log(obj);

                                        var newProposal = [];

                                        newProposal.push(obj.id);
                                        newProposal.push(obj.opportunities.length);
                                        newProposal.push(obj.discountedprice);
                                        newProposal.push(obj.cost);
                                        newProposal.push(obj.profit);
                                        newProposal.push(obj.status);
                                        newProposal.push(dateStr(obj.start));
                                        newProposal.push(dateStr(obj.end));
                                        newProposal.push(textBoolToText(obj.closed));
                                        newProposal.push(textBoolToText(obj.expired));
                                        newProposal.push(obj.discount);


                                        var strVar = "";
                                        strVar += "<button class=\"btn btn-raised btn-xs quick-details\" title=\"See Details\">";
                                        strVar += "							<i class=\"fa fa-eye fa-lg\"><\/i>";
                                        strVar += "						<\/button>";
                                        strVar += "						<button class=\"btn btn-raised btn-xs detail-prop\" title=\"Details of Proposal\">";
                                        strVar += "							<i class=\"fa fa-file-text-o fa-lg\"><\/i>";
                                        strVar += "						<\/button>";
                                        strVar += "						<button class=\"btn btn-raised btn-xs delete-prop\" title=\"Delete Proposal\">";
                                        strVar += "							<i class=\"fa fa-ban fa-lg\"><\/i>";
                                        strVar += "						<\/button>";


                                        newProposal.push(strVar);

                                        propTable.row.add(newProposal).draw();

                                        $(location).attr("href",detailPropUrl + obj.id);
                                    }


                                }, function () {
                                });

                        }

                        ajaxJsonGET(hasOpenProposalUrl,
                            {}, function (data) {

                                console.log(data);

                                if (is_numeric(data.length) && data.length > 0) {

                                    var header = "The Client has " + data.length + " Unclosed or Unexpired proposals. <br> Do you want to create another??"

                                    bootBoxConfirmation(header, function () {
                                        createNewProposal();

                                    }, function () {

                                    });

                                } else {


                                    createNewProposal();
                                }

                            }, function () {
                            });

                    }
                }],

            "renderer": "bootstrap",

            "scrollX": false,

        });

    /* Formatting function for row details - modify as you need */
    function format(d) {

        console.log(d);

        var strVar = "";
        strVar += "<div class=\"card\">";
        strVar += "		<h4 class=\"page-header\" >Quick Details<\/h4>";
        strVar += "							<table class=\"table\">";
        strVar += "								<tbody>";
        strVar += "									<tr>";
        strVar += "										<td><label>Number of Tasks<\/label><\/td>";
        strVar += "										<td><label>" + d[1] + "<\/label><\/td>";
        strVar += "									<\/tr>								";
        strVar += "									<tr>";
        strVar += "										<td><label>Price<\/label><\/td>";
        strVar += "										<td><label>" + d[2] + "<\/label><\/td>";
        strVar += "									<\/tr>								";
        strVar += "									<tr>";
        strVar += "										<td><label>Cost<\/label><\/td>";
        strVar += "										<td><label>" + d[3] + "<\/label><\/td>";
        strVar += "									<\/tr>								";
        strVar += "									<tr>";
        strVar += "										<td><label>Profit<\/label><\/td>";
        strVar += "										<td><label>" + d[4] + "<\/label><\/td>";
        strVar += "									<\/tr>							";
        strVar += "									<tr>";
        strVar += "										<td><label>Status<\/label><\/td>";
        strVar += "										<td><label>" + d[5] + "<\/label><\/td>";
        strVar += "									<\/tr>							";

        strVar += "									<tr>";
        strVar += "										<td><label>Start Date<\/label><\/td>";
        strVar += "										<td><label>" + dateStr(d[6]) + "<\/label><\/td>";
        strVar += "									<\/tr>							";
        strVar += "									<tr>";
        strVar += "										<td><label>End<\/label><\/td>";
        strVar += "										<td><label>" + dateStr(d[7]) + "<\/label><\/td>";
        strVar += "									<\/tr>							";
        strVar += "									<tr>";
        strVar += "										<td><label>Closed<\/label><\/td>";
        strVar += "										<td><label>" + textBoolToText(d[9]) + "<\/label><\/td>";
        strVar += "									<\/tr>							";
        strVar += "									<tr>";
        strVar += "										<td><label>Expired<\/label><\/td>";
        strVar += "										<td><label>" + textBoolToText(d[10]) + "<\/label><\/td>";
        strVar += "									<\/tr>							";
        strVar += "								<\/tbody>";
        strVar += "							<\/table>";
        strVar += "	<\/div>";

        return strVar;
    }

    propTable.on('click', '.quick-details', function () {
        var tr = $(this).closest('tr');
        var row = propTable.row(tr);

        var icon = $(this).children('i');

        if (row.child.isShown()) {

            icon.removeClass('fa-eye-slash').addClass('fa-eye');

            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {

            icon.removeClass('fa-eye').addClass('fa-eye-slash');

            // Open this row
            row.child(format(row.data())).show();
            tr.addClass('shown');
        }
    });

    // Script for delete proposal
    propTable.on('click', '.delete-prop', function () {
        var tr = $(this).closest('tr');
        var row = propTable.row(tr);
        var rowData = row.data();

        var url = deletePropUrl;

        var dataObj =
        {
            "id": is_numeric(rowData[0]) ? rowData[0].toString() : rowData[0]
        };

        ajaxJsonPost(url, dataObj, function (data) {
                showStatus(data.success, data.msg);

                if (data.success == true) {
                    row.remove().draw();
                }
            },

            function () {
                showStatus(false, "Failed to Delete");

            });

    });
    // Script for edit proposal
    propTable.on('click', '.detail-prop', function () {
        var tr = $(this).closest('tr');
        var row = propTable.row(tr);
        var rowData = row.data();

        var url = detailPropUrl + rowData[0];

        var dataObj =
        {
            "id": rowData[0]
        };


        $(location).attr('href', url);


//		ajaxJsonGET(url, dataObj, function(data)
//		{
//			bootbox.dialog(
//			{
//				title : "Modify Proposal " + rowData[0],
//				message :$('.modal-edit-prop').html(),
//				onEscape : true,
//				size:"large"
//			});
//			
//			
//			var modal = $(".bootbox-body");
//			
//			
//			for(var i in data.opportunities)
//			{
//				$('.bootbox-body .taskstable tbody:last-child').append('<tr>...</tr><tr>...</tr>')
//			
//			}
//		
//
//		},
//
//		function()
//		{
//			showStatus(false, "Failed to Delete");
//
//		});

    });

});