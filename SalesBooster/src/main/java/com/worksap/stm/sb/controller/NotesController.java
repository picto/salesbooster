package com.worksap.stm.sb.controller;


import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.worksap.stm.sb.domain.Note;
import com.worksap.stm.sb.service.NoteService;
import com.worksap.stm.sb.utils.SbLibrary;

@Controller
public class NotesController
{

	@Autowired
	NoteService notesService;

	@RequestMapping(value = "/tutorialsManager", method = RequestMethod.GET)
	public String tutorialsManager(HttpServletRequest request, Model model)
	{
		JSONObject obj = notesService.getTutorials();

		model.addAttribute("success", obj.get("success"));
		model.addAttribute("msg", obj.get("msg"));
		if (obj.get("data") != null)
			model.addAttribute("data", obj.get("data"));

		return SbLibrary.getPageFragment(request, "tutorials-manager");
	}

	@RequestMapping(value = "/tutorials", method = RequestMethod.GET)
	public String tutorials(HttpServletRequest request, Model model)
	{
		JSONObject obj = notesService.getTutorials();

		model.addAttribute("success", obj.get("success"));
		model.addAttribute("msg", obj.get("msg"));
		if (obj.get("data") != null)
			model.addAttribute("data", obj.get("data"));

		return SbLibrary.getPageFragment(request, "tutorials-salesrep");
	}


	@RequestMapping(value = "/createNote", method = RequestMethod.POST)
	public String postCreateNotePage(@ModelAttribute Note note, HttpServletRequest request, Model model) {


		System.err.println(note);

		JSONObject obj = notesService.createNote(note);




		return SbLibrary.getRedirectUrl("/tutorialsManager", "");

	}

	@RequestMapping(value = "/createTutorial", method = RequestMethod.GET)
	public String createTutorialGetPage( HttpServletRequest request, Model model) {


		model.addAttribute("backurl","/tutorialsManager");
		return SbLibrary.getPageFragment(request,"create-tutorial");

	}

	@RequestMapping(value = "/createNoteFromDetailsPage", method = RequestMethod.POST)
	public String postCreateNotePageFromDetailsPage(@ModelAttribute Note note, HttpServletRequest request, Model model)
	{

		JSONObject obj = notesService.createNote(note);

		if ((boolean) obj.get("success"))
		{
			String redirectUrl = "/getClientDetails/"+note.getClientid();
			

			return SbLibrary.getRedirectUrl("/getClientDetails/",note.getClientid());
		}

		else
		{
			model.addAttribute("success", obj.get("success"));
			model.addAttribute("msg", obj.get("msg"));
			if (obj.get("data") != null)
				model.addAttribute("data", obj.get("data"));

		}

		return SbLibrary.getPageFragment(request, "create-notes");
	}

	@RequestMapping(value = "/deleteNote", method = RequestMethod.POST)
	public @ResponseBody JSONObject deleteNote(@RequestBody JSONObject postData, HttpServletRequest request, Model model, BindingResult bindingResult)
	{

		JSONObject retObj = new JSONObject();

		if (bindingResult.hasErrors())
		{
			retObj.put("success", false);
			retObj.put("msg", "Failed to Delete");

		} else
		{

			
			String idStr = (String) postData.get("id");

			long id = Long.parseLong(idStr);

			retObj = notesService.deleteNote(id);

		}

		return retObj;

	}

}
