package com.worksap.stm.sb.service;

import java.util.List;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.worksap.stm.sb.domain.Notification;
import com.worksap.stm.sb.domain.User;
import com.worksap.stm.sb.repo.NotificationRepo;
import com.worksap.stm.sb.utils.SbLibrary;

@Service
public class NotificationService
{

	@Autowired
	NotificationRepo notificationRepo;
	@Autowired
	UserService userService;

	public JSONObject getNotifications()
	{
		List<Notification> notifications = null;
		JSONObject retObj;
		String username = SbLibrary.getCurrentUser().getUsername();

		try
		{
			notifications = (List<Notification>) notificationRepo.findBySrepresentative(username);

		} catch (Exception e)
		{
			// TODO: handle exception

			retObj = SbLibrary.getResponseObj(false, e.getMessage());

		}

		retObj = SbLibrary.getResponseObj(true, "All Notifications of " + username, notifications);

		return retObj;
	}

	public void create(String title, String msg, long clientid, String srep, String link)
	{
		Notification noti = new Notification();

		noti.setTitle(title);
		noti.setMsg(msg);
		noti.setSeen(false);
		noti.setClientid(clientid);
		noti.setSrepresentative(srep);
		noti.setLink(link);

		notificationRepo.save(noti);
	}
	
	public void notifyAllSalesRep(String title, String msg, long clientid, String link)
	{
		List<User> salesAssoscs = userService.getAllSalesAssocs();
		
		
		for(User sa: salesAssoscs)
		{
			this.create(title, msg, clientid, sa.getUsername(), link);
		}
	}

	public void removeNotifications(String srep, long clientid)
	{
		notificationRepo.removeBySrepresentativeAndClientid(srep, clientid);
	}

	public void changeUnseenNotificationsSrep(long clientid, String newRepresentative, String oldRepresentative)
	{
		notificationRepo.changeSrepOfUnseen(newRepresentative, oldRepresentative, clientid);
	}

	public JSONObject remove(long id)
	{
		JSONObject retObj = new JSONObject();

		try
		{
			notificationRepo.delete(id);
		} catch (Exception ex)
		{
			return SbLibrary.getResponseObj(false, ex.getMessage());

		}

		return SbLibrary.getResponseObj(true, "Deleted ", null);
	}

	public JSONObject countUnseenNotifications(String username)
	{

		int count = 0;

		try
		{
			count = notificationRepo.countBySrepresentativeAndSeen(username, false);

		} catch (Exception ex)
		{
			return SbLibrary.getResponseObj(false, ex.getMessage());
		}

		return SbLibrary.getResponseObj(true, "New Notification", count);
	}

	public Notification getNotificationById(long id) throws Exception
	{
		return notificationRepo.findOne(id);
	}

	public Notification setNotificationAsSeen(long id)
	{
		Notification notification = notificationRepo.findOne(id);

		notification.setSeen(true);
		notification.setShown(true);

		return notificationRepo.save(notification);

	}

	public JSONObject getUnshowedNotifications()
	{
		User curUser = SbLibrary.getCurrentUser().getUser();

		List<Notification> unshownNotis = notificationRepo.findBySrepresentativeAndShown(curUser.getUsername(), false);

		if (!unshownNotis.isEmpty())
			return SbLibrary.getResponseObj(true, "Unshown notifications", unshownNotis);
		return SbLibrary.getResponseObj(false, "No Notifications to shown");

	}

	public Notification setNotificationAsShown(long id)
	{
		Notification notification = notificationRepo.findOne(id);

		notification.setShown(true);

		return notificationRepo.save(notification);
	}

	public JSONObject getSingleUnshowedNotifications()
	{
		User curUser = SbLibrary.getCurrentUser().getUser();

		Notification unshownNotis = notificationRepo.findFirstBySrepresentativeAndShownByOrderByCreated(curUser.getUsername(), false);

		if (unshownNotis != null)
			return SbLibrary.getResponseObj(true, "Unshown notifications", unshownNotis);
		return SbLibrary.getResponseObj(false, "No Notifications to shown");
	}


	public void notifyManagers(String title, String msg, long clientid, String link) {


		List<User> managers = userService.getAllManagers();


		for(User manager: managers)
		{
			this.create(title, msg, clientid, manager.getUsername(), link);
		}
	}
}
