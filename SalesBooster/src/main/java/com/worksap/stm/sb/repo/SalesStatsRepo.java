package com.worksap.stm.sb.repo;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.worksap.stm.sb.domain.embedded.SalesStats;

import java.util.List;

public interface SalesStatsRepo extends PagingAndSortingRepository<SalesStats, Long>
{

    SalesStats findByProposalid(long id);


    @Query(value = "SELECT SUM(s.revenue) FROM salesstats s WHERE s.proposalstatus != 'LOST' AND MONTH(s.created) = MONTH(NOW())",nativeQuery = true)
    long totalOppThisMonth();


    @Query(value = "SELECT SUM(s.revenue) FROM salesstats s WHERE s.proposalstatus = 'WON' AND MONTH(s.created) = MONTH(NOW())",nativeQuery = true)
    long totalRevenueThisMonth();


    @Query(value = "SELECT SUM(s.profit) FROM salesstats s WHERE s.proposalstatus = 'WON' AND MONTH(s.created) = MONTH(NOW())",nativeQuery = true)
    long totalProfitThisMonth();


    @Query(value = "SELECT SUM(s.profit) FROM salesstats s WHERE s.proposalstatus != 'LOST' AND MONTH(s.created) = MONTH(NOW())",nativeQuery = true)
    long totalOppProfitThisMonth();

    @Query(value = "SELECT * FROM salesstats s WHERE s.proposalstatus = ?1 ORDER BY s.created DESC LIMIT ?2",nativeQuery = true)
    List<SalesStats> findNByProposalStatus(String status,int i);


    @Query(value = "SELECT * FROM salesstats s  ORDER BY s.created DESC LIMIT ?1",nativeQuery = true)
    List<SalesStats> findLatestNDeals(int i);

    List<SalesStats> findBySalesrep(String username);
}
