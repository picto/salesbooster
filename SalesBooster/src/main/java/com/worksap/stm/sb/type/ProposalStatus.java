package com.worksap.stm.sb.type;

public enum ProposalStatus
{

	
	OPEN,
	PROPOSED,
	WON,
	LOST
	
}
