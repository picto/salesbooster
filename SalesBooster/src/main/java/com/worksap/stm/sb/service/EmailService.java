package com.worksap.stm.sb.service;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;






import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.StringEscapeUtils;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.worksap.stm.sb.domain.EmailSent;
import com.worksap.stm.sb.domain.User;
import com.worksap.stm.sb.repo.EmailSentRepo;
import com.worksap.stm.sb.utils.SbLibrary;

@Service
public class EmailService
{

	@Autowired
	private JavaMailSender javaMailSender;

	@Autowired
	EmailSentRepo emailSentRepo;

	
	
	@Autowired
	UserService userService;
	
	public void sendSimpleEmail(String to, String from, String username, String password, String subject, String mailBody)
	{

		JavaMailSenderImpl mailSender = (JavaMailSenderImpl) javaMailSender;

		mailSender.setUsername(username);
		mailSender.setPassword(password);
		SimpleMailMessage mailMessage = new SimpleMailMessage();

		mailMessage.setTo(to);
		mailMessage.setFrom(from);
		mailMessage.setSubject(subject);
		mailMessage.setText(mailBody);

		try
		{
			mailSender.send(mailMessage);
		} catch (MailException me)
		{
			System.err.println(me.getMessage());
			me.printStackTrace();
		}
	}

	public boolean sendMimeEmail(String to, String from, String username, String password, String subject, String mailBody,
			Map<String, String> attachments, boolean html)
	{

		JavaMailSenderImpl mailSender = (JavaMailSenderImpl) javaMailSender;

		mailSender.setUsername(username);
		mailSender.setPassword(password);

		MimeMessage message = mailSender.createMimeMessage();

		// use the true flag to indicate you need a multipart message
		MimeMessageHelper helper;

		try
		{
			helper = new MimeMessageHelper(message, true);
			helper.setTo(to);
			helper.setFrom(from);
			helper.setSubject(subject);
			helper.setText(mailBody, html);

		
			// for(Map.Entry<String, String> me: attachments.entrySet())
			// {
			// FileSystemResource file = new FileSystemResource(new
			// File(me.getValue()));
			// helper.addAttachment(me.getKey(), file);
			// }
			//
			mailSender.send(message);
			
			return true;
			
			

		} catch (MessagingException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			return false;
		}

	}

	public JSONObject saveEmail(EmailSent email) 
	{

		User curUsr = SbLibrary.getCurrentUser().getUser();

		email.setSalesrep(curUsr.getUsername());
		
		email.setBody(StringEscapeUtils.escapeHtml4(email.getBody()));
		
		
		
		if(email.getReciever() == null || email.getReciever().isEmpty())
			return SbLibrary.getResponseObj(false, "Address can't be empty");
		
	
		if (email.getDatetime().isEmpty())
		{
			email.setSendtime(LocalDateTime.now());
			email = emailSentRepo.save(email);
			try
			{
				this.sendEmailNow(email);
			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
				return SbLibrary.getResponseObj(false,"Error in sending email");
			}

		} else
		{
			
			String dateTimeStr = email.getDatetime();
			
			email.setSendtime(LocalDateTime.parse(dateTimeStr,DateTimeFormat.forPattern("dd/MM/yyyy HH:mm")));
			email = emailSentRepo.save(email);
		}

		if (email.isSent())
			 return SbLibrary.getResponseObj(true,"Email has been Sent");

		return SbLibrary.getResponseObj(true,"Email will be sent on " + email.getSendtime());
	}

	private void sendEmailNow(EmailSent email) throws IOException
	{

		
		User sender = userService.getUserByUsername(email.getSalesrep());
		
		boolean isSent = this.sendMimeEmail(email.getReciever(), sender.getEmail(), sender.getEmail(), sender.getEmailPass(),
				email.getSubject(), StringEscapeUtils.unescapeHtml4(email.getBody()), null, true);
		
		

		email.setSent(isSent);
		emailSentRepo.save(email);
	}


	public void sendUnsentEmails() throws IOException
	{
		
		List<EmailSent> emails = emailSentRepo.findEmailsToSend();
		
		
		for(EmailSent e:emails)
		{
			this.sendEmailNow(e);
		}
		
	}

	public JSONObject getSentEmailByCLientid(Long id)
	{
		List<EmailSent> emails = emailSentRepo.findByClientid(id);
		
		return SbLibrary.getResponseObj(true, "Emails", emails);
	}

}
