package com.worksap.stm.sb.domain;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import com.worksap.stm.sb.domain.embededid.SalesRecordId;
import org.joda.time.LocalDateTime;
import org.springframework.data.annotation.Version;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;



@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class SalesRecord implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4590440338614011860L;

	@EmbeddedId
	SalesRecordId id;
	
	private long revenue;
	
	private long profit;
	
	private long opportunity;
	
	private long target;
	
	
	@DateTimeFormat(pattern = "dd/MM/yyyy-HH:mm")
	private LocalDateTime created;

	@DateTimeFormat(pattern = "dd/MM/yyyy-HH:mm")
	private LocalDateTime updated;

	@Version
	private long version;

	@PrePersist
	public void setCreationDate()
	{
		this.created = LocalDateTime.now();
	}

	@PreUpdate
	public void setChangeDate()
	{
		this.updated = LocalDateTime.now();
	}

}
