package com.worksap.stm.sb.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.worksap.stm.sb.domain.Proposal;
import com.worksap.stm.sb.domain.User;
import com.worksap.stm.sb.service.ClientService;
import com.worksap.stm.sb.service.ProposalService;
import com.worksap.stm.sb.service.StatsService;
import com.worksap.stm.sb.service.UserService;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.worksap.stm.sb.utils.SbLibrary;

import java.io.IOException;
import java.util.List;

@Controller
@SessionAttributes("name")
public class IndexController
{
	private static final String APP_NAME = "Moppers Cleaning Services";

	@Autowired
	ProposalService proposalService;


	@Autowired
	UserService userService;

	@Autowired
	StatsService statsService;

	@Autowired
	ClientService clientService;

	@RequestMapping(value =
	{ "/", "/index" })
	public String index(Model model, HttpSession session)
	{


		return SbLibrary.getRedirectUrl("/dashboard","");
	}

	@RequestMapping(value =
	{ "/dashboard" })
	public String welcome(HttpServletRequest request, Model model )
	{

		model.addAttribute("appName", APP_NAME);


		List<Proposal> proposals = proposalService.getLatestProposals();

		model.addAttribute("proposals",proposals);


		return SbLibrary.getPageFragment(request, "dashboard");
	}


	@RequestMapping(value ="/usersettings")
	public String usersettings(HttpServletRequest request, Model model, HttpSession session) {

		model.addAttribute("appName", APP_NAME);

		return SbLibrary.getPageFragment(request, "settings");
	}


	@RequestMapping(value ="/profile")
	public String profile(HttpServletRequest request, Model model, HttpSession session) {

		model.addAttribute("appName", APP_NAME);


		User user = SbLibrary.getCurrentUser().getUser();

		if(user.getRoles().equals("BM"))
		{
			JSONObject statsObj = statsService.getStats();

			JSONObject clientObj = clientService.getClientStats();

//			JSONObject proposalObj = proposalService.getProposalStats();
			JSONObject proposalObj = statsService.getProposalStats();

			model.addAttribute("statsObj",statsObj);
			model.addAttribute("clientObj",clientObj);
			model.addAttribute("proposalObj",proposalObj);

		}else if(user.getRoles().equals("SA"))
		{
			JSONObject statsObj = statsService.getStats(user.getUsername());

			JSONObject clientObj = clientService.getClientStats(user.getUsername());

//			JSONObject proposalObj = proposalService.getProposalStats(user.getUsername());
			JSONObject proposalObj = statsService.getProposalStats(user.getUsername());

			model.addAttribute("statsObj",statsObj);
			model.addAttribute("clientObj",clientObj);
			model.addAttribute("proposalObj",proposalObj);
		}




		return SbLibrary.getPageFragment(request, "profile");
	}


	@PreAuthorize("hasAuthority('BM')")
	@RequestMapping(value ="/profilesa/{username}")
	public String profileUsername(@PathVariable String username, HttpServletRequest request, Model model, HttpSession session) throws IOException {


		User user = userService.getUserByUsername(username);


		JSONObject statsObj = statsService.getStats(username);

		JSONObject clientObj = clientService.getClientStats(username);

//		JSONObject proposalObj = proposalService.getProposalStats(username);

		JSONObject proposalObj = statsService.getProposalStats(username);

		model.addAttribute("statsObj",statsObj);
		model.addAttribute("clientObj",clientObj);
		model.addAttribute("proposalObj",proposalObj);
		model.addAttribute("user",user);


		return SbLibrary.getPageFragment(request, "profile-salesrep");
	}

}
