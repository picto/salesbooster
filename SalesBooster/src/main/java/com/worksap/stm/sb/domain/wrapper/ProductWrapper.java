package com.worksap.stm.sb.domain.wrapper;

import java.util.List;

import com.worksap.stm.sb.domain.Product;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ProductWrapper
{
	private String msg;
	private Product product;

}
