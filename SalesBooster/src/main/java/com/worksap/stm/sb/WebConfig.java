package com.worksap.stm.sb;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import uk.co.gcwilliams.jodatime.thymeleaf.JodaTimeDialect;

import com.worksap.stm.sb.interceptor.AccessControlInterceptor;
@Configuration
@EnableWebMvc
public class WebConfig extends WebMvcConfigurerAdapter {


	@Autowired
	AccessControlInterceptor accessControlInterceptor;

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
	}


	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(accessControlInterceptor);
	}

	@Bean
	public MessageSource messageSource() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasename("classpath:/messages/messages");
		return messageSource;
	}
	
	
	@Bean
	public JodaTimeDialect jodaTimeDialect() {
	    return new JodaTimeDialect();
	}

}
