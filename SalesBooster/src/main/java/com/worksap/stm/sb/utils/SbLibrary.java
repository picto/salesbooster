package com.worksap.stm.sb.utils;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.Base64Utils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.worksap.stm.sb.domain.ProductConfig;
import com.worksap.stm.sb.entity.CurrentUser;




public class SbLibrary
{

	public static boolean isAjax(HttpServletRequest request)
	{
		String requestedWithHeader = request.getHeader("X-Requested-With");
		return "XMLHttpRequest".equals(requestedWithHeader);
	}

	public static String getPageFragment(HttpServletRequest request, String pageName)
	{
		StringBuffer sb = new StringBuffer(pageName);
		if (SbLibrary.isAjax(request))
		{
			return new String(sb.append("::ajax-container"));
		} else
		{
			return pageName;
		}
	}

	public static String encodeMailPass(String rawPwd)
	{
		return Base64Utils.encodeToString(rawPwd.getBytes());
	}

	public static String decodeMailPass(String encoded)
	{
		return new String(Base64Utils.decodeFromString(encoded));
	}

	public static String trimRegex(String s)
	{
		return s.replaceAll("(,$)", "");
	}

	public static String getURLWithContextPath(HttpServletRequest request)
	{
		return request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
	}

	public static String ensureHasProtocol(String a_url, String protocol)
	{
		if (!a_url.startsWith(protocol))
		{
			return protocol + a_url;
		}
		return a_url;
	}

	public static CurrentUser getCurrentUser()
	{
		CurrentUser principal = (CurrentUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		return principal;

	}

	public static JSONObject getResponseObj(boolean status, String msg)
	{
		return getResponseObj(status, msg, null);
	}

	public static JSONObject getResponseObj(boolean status, String msg, Object obj)
	{
		JSONObject retObj = new JSONObject();

		retObj.put("success", status);
		retObj.put("msg", msg);
		retObj.put("data", obj);

		return retObj;

	}

	public static String getRedirectUrl(String path, String varToAppend)
	{

		String redirectUrl = path + varToAppend;

		return "redirect:" + redirectUrl;
	}

	public static String getRedirectUrl(String path, long clientid)
	{
		String redirectUrl = path + clientid;

		return "redirect:" + redirectUrl;
	}

	public static String getUrl(String path, long append)
	{
		String redirectUrl = path + append;

		return redirectUrl;
	}

	public static String getFullUrl(String relativeUrl)
	{
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

		return request.getContextPath() + relativeUrl;
	}


}
