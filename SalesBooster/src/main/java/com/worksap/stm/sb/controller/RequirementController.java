package com.worksap.stm.sb.controller;

import javax.servlet.http.HttpServletRequest;

import com.worksap.stm.sb.RequirementInfo;
import com.worksap.stm.sb.domain.Client;
import com.worksap.stm.sb.domain.Product;
import com.worksap.stm.sb.domain.Requirement;
import com.worksap.stm.sb.domain.embedded.Week;
import com.worksap.stm.sb.domain.embedded.WorkingShift;
import com.worksap.stm.sb.domain.wrapper.RequirementWrapper;
import com.worksap.stm.sb.utils.SbLibrary;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import com.worksap.stm.sb.service.ClientService;
import com.worksap.stm.sb.service.ProductService;
import com.worksap.stm.sb.service.RequirementService;
import com.worksap.stm.sb.service.TimelineService;

import java.text.DateFormat;
import java.util.List;

@Controller
public class RequirementController
{
	@Autowired
	ClientService clientService;

	@Autowired
	ProductService productService;

	@Autowired
	RequirementService requirementService;
	
	
	@Autowired
	TimelineService timelineService;

	@RequestMapping(value = "/createRequirement/{clientid}", method = RequestMethod.POST)
	public String createRequirement(@PathVariable("clientid") long clientid, @ModelAttribute RequirementWrapper requirementWrapper,
			HttpServletRequest request)
	{

		String curUsername = SbLibrary.getCurrentUser().getUser().getName();

		JSONObject clientObj = clientService.getClientById(clientid);
		JSONObject proObject = productService.getServiceByCatAndTask(requirementWrapper.getSurface(), requirementWrapper.getTask());


		System.out.println(requirementWrapper);
		System.out.println(clientObj);
		System.out.println(proObject);

		if ((boolean) clientObj.get("success") && (boolean) proObject.get("success"))
		{
			Requirement requirement = new Requirement();
			requirement.setClient((Client) clientObj.get("data"));
			requirement.setProduct((Product) proObject.get("data"));
			requirement.setArea(requirementWrapper.getArea());
			requirement.setWeek(requirementWrapper.getWeek());
			requirement.setShift(requirementWrapper.getShift());
			requirement.setRequired(requirementWrapper.isRequired());
			requirement.setTimesinshift(requirementWrapper.getTimesinshift());

			requirementService.create(requirement);

			timelineService.create(clientid,"New Requirement!! ", "Added a New Requirement by "+curUsername);
		}
		return SbLibrary.getRedirectUrl("/setRequirement/", clientid);

	}

	@RequestMapping(value = "/editRequirement/{reqid}", method = RequestMethod.POST)
	public @ResponseBody JSONObject editRequirement(@PathVariable("reqid") long reqid, @RequestBody JSONObject postData,
			HttpServletRequest request)
	{

		JSONObject reqObj = requirementService.getById(reqid);

		System.out.println(postData);

		if ((boolean) reqObj.get("success"))
		{
			Requirement requirement = (Requirement) reqObj.get("data");

            System.err.println(requirement);


            requirement.setId(reqid);
            requirement.setArea(Float.parseFloat((String) postData.get("area")));
            requirement.setTimesinshift(Integer.parseInt((String) postData.get("timesinshift")));


            if(postData.containsKey("required"))
                requirement.setRequired((Boolean) postData.get("required"));

            WorkingShift workingShift = new WorkingShift();


            if(postData.containsKey("shift.day"))
                workingShift.setDay((Boolean) postData.get("shift.day"));

            if(postData.containsKey("shift.night"))
                workingShift.setNight((Boolean) postData.get("shift.night"));

            requirement.setShift(workingShift);

            Week week = new Week();

            if(postData.containsKey("week.sunday"))
                week.setSunday((Boolean) postData.get("week.sunday"));

            if(postData.containsKey("week.monday"))
                week.setMonday((Boolean) postData.get("week.monday"));

            if(postData.containsKey("week.tuesday"))
                week.setTuesday((Boolean) postData.get("week.tuesday"));

            if(postData.containsKey("week.wednesday"))
                week.setWednesday((Boolean) postData.get("week.wednesday"));


            if(postData.containsKey("week.thursday"))
                week.setThursday((Boolean) postData.get("week.thursday"));

            if(postData.containsKey("week.friday"))
                week.setFriday((Boolean) postData.get("week.friday"));

            if(postData.containsKey("week.saturday"))
                week.setSaturday((Boolean) postData.get("week.saturday"));


            requirement.setWeek(week);

			System.err.println(requirement);

			return requirementService.update(requirement);

		}

		return SbLibrary.getResponseObj(false, "Error Happend. Try again");

	}

	@RequestMapping(value = "/setRequirement/{clientid}", method = RequestMethod.GET)
	public String getSetRequirement(@PathVariable("clientid") long clientid, HttpServletRequest request, Model model)
	{

		JSONObject clientObj = clientService.getClientById(clientid);

		Client client = null;

		model.addAttribute("success", clientObj.get("success"));
		model.addAttribute("msg", clientObj.get("msg"));
		model.addAttribute("clientid",clientid);

		if ((boolean) clientObj.get("success"))
		{
			model.addAttribute("client", clientObj.get("data"));
			client = (Client) clientObj.get("data");
		}

		JSONObject reqObj = requirementService.getRequirementsByClient(clientid);

		model.addAttribute("requirements", reqObj.get("data"));





		JSONObject recObj = productService.getTagMatchedServices(client.getIndustry(),(List<Requirement>) reqObj.get("data"));


		model.addAttribute("recsByTags",recObj.get("data"));


		recObj = productService.getSimilarServices((List<Requirement>) reqObj.get("data"));


		model.addAttribute("recsByTypes",recObj.get("data"));

		recObj = productService.getPurchaseMatchedServices((List<Requirement>) reqObj.get("data"));


		model.addAttribute("recsByPur",recObj.get("data"));


		model.addAttribute("backurl", SbLibrary.getUrl("/getClientDetails/", clientid));

		return SbLibrary.getPageFragment(request, "collect-req");
	}

	@RequestMapping(value = "/deleteRequirement", method = RequestMethod.POST)
	public @ResponseBody JSONObject deleteRequirement(@RequestBody JSONObject postData, HttpServletRequest request, Model model,
			BindingResult bindingResult)
	{

		JSONObject retObj = new JSONObject();

		if (bindingResult.hasErrors())
		{
			retObj.put("success", false);
			retObj.put("msg", "Failed to Delete");

		} else
		{
			// Create in DB and return

//			System.err.println(postData);

			String idStr = (String) postData.get("id");

			long id = Long.parseLong(idStr);

			retObj = requirementService.delete(id);

		}

		return retObj;

	}


    @RequestMapping(value = "/addRequirementInfo/{clientid}", method = RequestMethod.POST)
	public @ResponseBody
    JSONObject addRequirementInfo(@PathVariable long clientid, @RequestBody JSONObject postData, HttpServletRequest request, Model model)
	{


		RequirementInfo requirementInfo = new RequirementInfo();
        requirementInfo.setClientid(clientid);


		requirementInfo.setStart(LocalDate.parse((String) postData.get("start")));
		requirementInfo.setWeeks(Long.parseLong((String) postData.get("weeks")));



        return requirementService.createReqInfo(requirementInfo);


	}


    @RequestMapping(value = "/deleteReqInfo/{id}", method = RequestMethod.GET)
	public @ResponseBody
    JSONObject deleteReqInfo(@PathVariable long id, HttpServletRequest request, Model model)
	{


		RequirementInfo requirementInfo = new RequirementInfo();
        requirementInfo.setId(id);


        return requirementService.deleteReqInfo(requirementInfo);


	}




}
