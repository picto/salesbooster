package com.worksap.stm.sb.dao.spec;

import com.worksap.stm.sb.dto.FileDto;

public interface FileDao {

	public int addFile(FileDto fileDto);
}
