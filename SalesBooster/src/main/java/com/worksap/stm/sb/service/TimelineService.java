package com.worksap.stm.sb.service;

import java.util.List;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.worksap.stm.sb.domain.Timeline;
import com.worksap.stm.sb.repo.TimelineRepo;
import com.worksap.stm.sb.utils.SbLibrary;

@Service
public class TimelineService
{

	@Autowired
	TimelineRepo timelineRepo;

	public void create(long clientid, String srep, String heading, String msg)
	{

		Timeline timeline = new Timeline();

		timeline.setClientid(clientid);
		timeline.setHeading(heading);
		timeline.setSrepresentative(srep);
		timeline.setMsg(msg);

		try
		{
			timelineRepo.save(timeline);
		} catch (Exception ex)
		{
			System.err.println(ex.getMessage());
		}

	}

	public void create(long clientid, String heading, String msg)
	{

		Timeline timeline = new Timeline();

		timeline.setClientid(clientid);
		timeline.setSrepresentative(SbLibrary.getCurrentUser().getUsername());
		timeline.setMsg(msg);
		timeline.setHeading(heading);

		try
		{
			timeline = timelineRepo.save(timeline);
			
		} catch (Exception ex)
		{
			System.err.println(ex.getMessage());
		}

		
		//System.err.println(timeline);
	}

	public JSONObject getTimelineEvents(long clientid)
	{
		List<Timeline> events = null;

		try
		{
			events = timelineRepo.findByClientidOrderByCreated(clientid);
		} catch (Exception ex)
		{
			return SbLibrary.getResponseObj(false, ex.getMessage());
		}

		return SbLibrary.getResponseObj(true, "Timeline events", events);

	}

}
