package com.worksap.stm.sb.repo;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.worksap.stm.sb.domain.SalesRecord;
import com.worksap.stm.sb.domain.embededid.SalesRecordId;

import java.util.List;

public interface SalesRecordRepo extends PagingAndSortingRepository<SalesRecord, SalesRecordId>
{

    @Query(value = "select * from salesrecord order by salesrecord.yearOfCentury DESC , salesrecord.monthOfYear DESC LIMIT ?1 ",nativeQuery = true)
    List<SalesRecord> findSalesRecordMonthLimit(int i);

    @Query(value = "select s.created,s.updated,s.version,s.monthOfYear,s.target,SUM(s.opportunity) as opportunity ,SUM(s.profit) as profit,SUM(s.revenue) as revenue, s.yearOfCentury from salesrecord s  GROUP BY s.yearOfCentury ORder by s.yearOfCentury DESC LIMIT ?1",nativeQuery = true)
    List<SalesRecord> findSalesRecordYear(int i);
}
