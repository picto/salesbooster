package com.worksap.stm.sb.domain;

import com.worksap.stm.sb.domain.embededid.ProductRelationId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.common.util.StringHelper;

import javax.persistence.*;
import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by mamun on 8/1/2016.
 */


@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity

public class ProductRelation implements Serializable {


    @EmbeddedId
    ProductRelationId id;

    private long times;


    @Type(type = "text")
    private String ids = "";

    @Transient
    Set<Long> idset;

    private long distnct;





}
