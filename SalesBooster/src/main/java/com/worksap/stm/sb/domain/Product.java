package com.worksap.stm.sb.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.validator.constraints.NotBlank;

import com.worksap.stm.sb.type.Priority;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames =
{ "surface", "task" }))
public class Product extends BaseEntity
{

	@NotBlank
	private String surface;

	@NotBlank
	private String task;

	@Column(name = "fixed", columnDefinition = "Decimal(10,4) default '00.00'")
	private float fixed;

	@Column(name = "var", columnDefinition = "Decimal(10,4) default '00.00'")
	private float var;



	private String relatedservice;
	
	
	@Enumerated(EnumType.STRING)
	@Column(nullable=false)
	private Priority priority = Priority.LOW;
	
	
	@Column(length=1023)
	private String tags;


	public String getProductName()
	{
		String surface  = this.getSurface();
		return this.getTask()+"  "+surface;
	}
}
