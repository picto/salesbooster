package com.worksap.stm.sb.dao.spec;

import java.io.IOException;
import java.util.List;

import com.worksap.stm.sb.dto.RoleDto;
import com.worksap.stm.sb.dto.TeamDto;
import com.worksap.stm.sb.dto.UserDto;

public interface UserDao {
	
	List<UserDto> getUsers(String cond) throws IOException;
	
	List<RoleDto> getRoles(String username) throws IOException;
	List<TeamDto> getTeams(String username) throws IOException;

	UserDto getUserByUsername(String username) throws IOException;
}
