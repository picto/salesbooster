package com.worksap.stm.sb.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.worksap.stm.sb.utils.SbLibrary;

@Controller
public class LeadGeneration
{

	@RequestMapping("/OpRoute")
	public String OptimalRoute(HttpServletRequest request)
	{

		return SbLibrary.getPageFragment(request, "optimalRoute");
	}

}
