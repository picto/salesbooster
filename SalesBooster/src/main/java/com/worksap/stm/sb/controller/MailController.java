package com.worksap.stm.sb.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.worksap.stm.sb.domain.Proposal;
import com.worksap.stm.sb.service.ProposalService;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.worksap.stm.sb.domain.EmailSent;
import com.worksap.stm.sb.domain.User;
import com.worksap.stm.sb.service.ClientService;
import com.worksap.stm.sb.service.EmailService;
import com.worksap.stm.sb.utils.SbLibrary;

@Controller
public class MailController
{

	@Autowired
	private EmailService emailService;

	@Autowired
	private ClientService clientService;
    @Autowired
    ProposalService proposalService;

    @RequestMapping(value = "/sendEmail/{clientid}", method = RequestMethod.GET)
	public String Compose(@PathVariable long clientid,@RequestParam String backurl,@RequestParam String mailtext, HttpServletRequest request, Model model)
	{

		JSONObject cObj = clientService.getClientById(clientid);
		model.addAttribute("success", cObj.get("success"));
		model.addAttribute("msg", cObj.get("msg"));
		if (cObj.get("data") != null)
		{
			model.addAttribute("data", cObj.get("data"));
		}

		model.addAttribute("pretext",mailtext);
		
		model.addAttribute("backurl", backurl);
		return SbLibrary.getPageFragment(request, "send-email");
	}

	@RequestMapping(value = "/mailProposal/{id}", method = RequestMethod.GET)
	public String mailProposal(@PathVariable long id, HttpServletRequest request, Model model)
	{

        Proposal proposal = proposalService.getProposalById(id);

        model.addAttribute("pretext", proposalService.formattedProposalForMail(id));

        model.addAttribute("backurl", SbLibrary.getUrl("/detailProposal/", id));


        JSONObject cObj = clientService.getClientById(proposal.getClientid());
		model.addAttribute("success", cObj.get("success"));
		model.addAttribute("msg", cObj.get("msg"));
		if (cObj.get("data") != null)
		{
			model.addAttribute("data", cObj.get("data"));
		}



		return SbLibrary.getPageFragment(request, "send-email");
	}

	@RequestMapping(value = "/sendEmail/{clientid}", method = RequestMethod.POST)
	public @ResponseBody JSONObject sendEmail(@PathVariable long clientid, @ModelAttribute EmailSent email, HttpServletRequest request, Model model) throws IOException
	{

		email.setClientid(clientid);

		return emailService.saveEmail(email);

	}

	@RequestMapping(value = "/sendMail", method = RequestMethod.GET)
	public void sendMail(@ModelAttribute("backurl") String backurl, Model model, HttpSession session)
	{
		
		model.addAttribute("backurl", backurl);

	}

}
