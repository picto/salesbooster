package com.worksap.stm.sb.service;

import com.worksap.stm.sb.domain.Opportunity;
import com.worksap.stm.sb.domain.Product;
import com.worksap.stm.sb.domain.ProductRelation;
import com.worksap.stm.sb.domain.Proposal;
import com.worksap.stm.sb.domain.embededid.ProductRelationId;
import com.worksap.stm.sb.domain.wrapper.ProductWrapper;
import com.worksap.stm.sb.repo.ProductRelationRepo;
import com.worksap.stm.sb.repo.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by mamun on 8/1/2016.
 */


@Service
public class ProductRelationService {

    @Autowired
    ProductRelationRepo productRelationRepo;
    
    @Autowired
    ProductRepo productRepo;


    public void updateFromProposal(Proposal proposal) {

        List<Opportunity> opportunityList = proposal.getOpportunities();


        for (Opportunity op1 : opportunityList) {

            long p1 = op1.getProduct().getId();

            for (Opportunity op2 : opportunityList) {

                long p2 = op2.getProduct().getId();

                if (p1 != p2) {
                    this.updateProductRelation(p1, p2, proposal.getClientid());
                }
            }
        }

    }

    public ProductRelation preUpsert(ProductRelation relation) {
        String ids = "";

        if (relation.getIdset() != null) {
            for (long id : relation.getIdset()) {
                ids += id + ";";
            }

            ids = ids.substring(0, ids.length() - 1);
        }
        relation.setIds(ids);

        return relation;
    }


    public ProductRelation postLoad(ProductRelation relation) {
        String[] idStrs = relation.getIds().split(";");

        Set<Long> idSet = new LinkedHashSet<Long>();

        for (String idStr : idStrs) {
            if (!idStr.isEmpty())
                idSet.add(Long.parseLong(idStr));
        }

        relation.setIdset(idSet);

        return relation;
    }


    public ProductRelation save(ProductRelation relation) {
        relation = this.preUpsert(relation);

        return this.productRelationRepo.save(relation);

    }


    public ProductRelation findOne(ProductRelation relation) {
        relation = this.productRelationRepo.findOne(relation.getId());

        if (relation != null)
            relation = this.postLoad(relation);

        return relation;
    }


    private void updateProductRelation(long p1, long p2, long clientid) {

        ProductRelationId id = new ProductRelationId(p1, p2);

        ProductRelation relation = new ProductRelation();
        relation.setId(id);
        relation = this.findOne(relation);

        if (relation != null) {
            relation.setTimes(relation.getTimes() + 1);
            Set<Long> ids = relation.getIdset();

            if (ids != null && !ids.contains(clientid)) {
                ids.add(clientid);
            }
            relation.setIdset(ids);
            relation.setDistnct(ids.size());
        } else {
            relation = new ProductRelation();
            relation.setId(id);
            relation.setTimes(1);
            Set<Long> ids = new LinkedHashSet<Long>();
            ids.add(clientid);
            relation.setIdset(ids);
            relation.setDistnct(1);
        }

        this.save(relation);

    }

    public List<ProductWrapper> findRelatedProductsTop(Product product, int i) {


        List<ProductRelation> relations = this.productRelationRepo.findTopRelatedProducts(product.getId(), 10);


        List<ProductWrapper> productWrapperList = new LinkedList<>();

        for (ProductRelation relation : relations) {

            long proId = relation.getId().getRelatedproductid();

            Product p = productRepo.findOne(proId);

            String msg = relation.getDistnct()+ " Client(s) who purchased " + product.getProductName() + "<br/> also purchased this Service";

            productWrapperList.add(new ProductWrapper(msg, p));

        }

        return productWrapperList;

    }
}
