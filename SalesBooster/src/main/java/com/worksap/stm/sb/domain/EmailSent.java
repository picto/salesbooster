package com.worksap.stm.sb.domain;



import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.Transient;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper=true)
@Data
@Entity
public class EmailSent extends BaseEntity
{

	private long clientid;
	

	private String salesrep;;
	
	private String reciever;
	
	private String subject;
	
	
	@Column(name="mailtext")
	@Type(type="text")
	private String body;
	
	@Transient
	private String datetime;
	
	@DateTimeFormat(pattern = "dd/MM/yyyy-HH:mm")
	private LocalDateTime sendtime;
	
	
	private boolean sent = false;
}
