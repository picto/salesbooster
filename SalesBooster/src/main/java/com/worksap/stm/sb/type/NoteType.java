package com.worksap.stm.sb.type;

public enum NoteType
{
	GENERAL,
	CALL,
	MEETING,
	EMAIL,
	REQUIREMENT,
	PROPOSAL,
	NEGOTIATION
}
