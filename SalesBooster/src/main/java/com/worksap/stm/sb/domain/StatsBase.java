package com.worksap.stm.sb.domain;


import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;

import com.worksap.stm.sb.type.StatsType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper=true)

@Data
@MappedSuperclass
   
public abstract class StatsBase extends BaseEntity
{

	private String salesrep;
	
	private long clientid;

	@Enumerated(EnumType.STRING)
	private StatsType type;
	
}
