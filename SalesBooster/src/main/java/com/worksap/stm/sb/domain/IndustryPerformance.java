package com.worksap.stm.sb.domain;

import com.worksap.stm.sb.domain.embededid.IndustryPerformanceId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by mamun on 13/1/2016.
 */





@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class IndustryPerformance {



    @Id
    private IndustryPerformanceId id;

    private long amount;
    private long noofdeals;

}
