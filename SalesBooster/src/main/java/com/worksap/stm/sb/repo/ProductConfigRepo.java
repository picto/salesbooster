package com.worksap.stm.sb.repo;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.worksap.stm.sb.domain.ProductConfig;

public interface ProductConfigRepo extends PagingAndSortingRepository<ProductConfig, Long>
{

}
