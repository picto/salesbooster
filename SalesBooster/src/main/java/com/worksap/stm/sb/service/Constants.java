package com.worksap.stm.sb.service;

import java.util.concurrent.atomic.AtomicReference;

import org.springframework.stereotype.Component;

@Component
public class Constants {

    private static AtomicReference<Constants> INSTANCE = new AtomicReference<Constants>();

    public Constants() {
        final Constants previous = INSTANCE.getAndSet(this);
        if(previous != null)
            throw new IllegalStateException("Second singleton " + this + " created after " + previous);
    }

    public static Constants getInstance() {
        return INSTANCE.get();
    }

}