package com.worksap.stm.sb.repo;

import java.util.List;

import com.worksap.stm.sb.type.TaskType;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

import com.worksap.stm.sb.domain.Task;
import com.worksap.stm.sb.type.ClientStatusType;
import com.worksap.stm.sb.type.TaskStatus;

public interface TaskRepo extends PagingAndSortingRepository<Task, Long>
{

	@Query(value = "SELECT * FROM task t WHERE t.clientid = ?1 ORDER BY t.id DESC", nativeQuery = true)
	List<Task> findByClientid(long clientid);

	@Query(value = "SELECT * FROM task t WHERE t.srepresentative = ?1 OR t.teammeeting = true ORDER BY t.id DESC", nativeQuery = true)
	List<Task> findBySrepresentative(String user);

	@Query(value = "select task.* from task where task.clientid in (select distinct client.id from  client left join delegates on client.id = delegates.Client\n" +
			"where client.srepresentative = ?1 or delegates.delegates = ?1) or" +
			" task.teammeeting = true order by task.id", nativeQuery = true)
	List<Task> findAllTaskForSrepresentative(String user);

	@Query(value = "SELECT COUNT(*) FROM task t WHERE ((t.srepresentative = ?1 AND t.status = 'DUE') OR t.teammeeting = true) AND TO_DAYS( t.start ) - TO_DAYS( NOW() ) = 0", nativeQuery = true)
	int countDueTaskToday(String user);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query(value = "UPDATE task t SET t.srepresentative = ?1 WHERE t.srepresentative = ?2 AND t.clientid = ?3 ", nativeQuery = true)
	void changeSrep(String newsrep, String oldsrep, long clientid);
	
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query(value = "UPDATE task t SET t.srepresentative = ?1 WHERE  t.clientid = ?2 AND t.status = ?3 AND t.missed = false", nativeQuery = true)
	void changeSrepOfDueTasks(String newsrep, long clientid,String status);


	List<Task> findByClientidAndClientstatus(long clientid, ClientStatusType clientstatus);

	@Query(value = "SELECT * FROM task t WHERE t.status = 'COMPLETED' AND (TO_DAYS(NOW()) - TO_DAYS(t.end)) > ?1", nativeQuery = true)
	List<Task> findCompletedOldTasks(long days);

	@Query(value = "SELECT * FROM task t WHERE t.status = 'DUE' and t.teammeeting = false AND (NOW() > t.end) ", nativeQuery = true)
	List<Task> findMissedTasks();

	List<Task> findByClientidAndType(long clientid, TaskType type);


	@Query(value = "SELECT * FROM task t WHERE t.status = 'DUE' and t.teammeeting = true AND (NOW() > t.end) ", nativeQuery = true)
	List<Task> findPassedTeamMeetings();


	@Query(value = "select * from task\n" +
			" where task.clientid = ?1 order by status desc, (task.end - now()) desc ,(now() - task.start)  limit 1",nativeQuery = true)
	Task closestTask(long clientid);
}
