package com.worksap.stm.sb.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

import com.worksap.stm.sb.domain.Notification;

public interface NotificationRepo extends PagingAndSortingRepository<Notification, Long>
{

	@Query(value="SELECT * FROM notification n WHERE n.srepresentative = ?1 ORDER BY n.seen, n.created DESC", nativeQuery=true)
	List<Notification> findBySrepresentative(String srep);
	
	int countBySrepresentativeAndSeen(String srep,boolean seen);
	
	void removeBySrepresentativeAndClientid(String srep,long clientid);
	
	@Modifying
	@Transactional
	@Query(value="UPDATE notification n SET n.srepresentative = ?1 WHERE n.srepresentative = ?2 AND n.clientid = ?3",nativeQuery=true)
	void changeSrepresentative(String newRep, String oldRep, long clientid);
	
	
	@Modifying
	@Transactional
	@Query(value="UPDATE notification n SET n.srepresentative = ?1 WHERE n.srepresentative = ?2 AND n.clientid = ?3 AND n.seen = false",nativeQuery=true)
	void changeSrepOfUnseen(String newRep, String oldRep, long clientid);


	List<Notification> findBySrepresentativeAndShown(String username, boolean b);

	
	@Query(value="SELECT * FROM notification n WHERE n.srepresentative = ?1 AND n.shown = ?2 ORDER BY n.created LIMIT 1",nativeQuery=true)
	Notification findFirstBySrepresentativeAndShownByOrderByCreated(String username, boolean b);


}

