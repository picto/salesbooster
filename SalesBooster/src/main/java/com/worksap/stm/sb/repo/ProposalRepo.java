package com.worksap.stm.sb.repo;

import java.util.List;

import com.worksap.stm.sb.type.ProposalStatus;
import org.joda.time.LocalDate;
import org.springframework.transaction.annotation.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;


import com.worksap.stm.sb.domain.Proposal;

public interface ProposalRepo extends PagingAndSortingRepository<Proposal, Long>
{
	List<Proposal> findByClientid(long clientid);

	@Query(value="SELECT * FROM proposal p WHERE p.clientid = ?1 AND (p.closed = false OR p.expired = false)",nativeQuery=true)
	List<Proposal> findOpenAndUnexpiredProposals(long clientid);

	
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query(value="UPDATE proposal p SET p.expired = true WHERE WEEK(NOW()) - WEEK(p.updated) > p.validfor",nativeQuery=true)
	void setExpireds();

	List<Proposal> findByStatus(ProposalStatus proposed);


	@Query(value = "SELECT * FROM proposal p ORDER BY p.id DESC LIMIT ?1",nativeQuery = true)
	List<Proposal> findFirstN(int limit);



	@Query(value = "SELECT * FROM proposal p where p.salesrep = ?1 ORDER BY p.id DESC LIMIT ?2",nativeQuery = true)
	List<Proposal> findSalesrep(String salesrep,int i);

	@Query(value = "SELECT * FROM proposal p where p.salesrep = ?1 ORDER BY p.id DESC",nativeQuery = true)
	List<Proposal> findSalesrep(String username);

	@Query(value = "SELECT SUM(numberofcleaners) From proposal where (start < ?1) And (end > ?1)",nativeQuery = true)
	Long getEmployedCleaners(LocalDate start);
}
