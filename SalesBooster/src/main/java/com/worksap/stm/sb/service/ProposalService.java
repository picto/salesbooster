package com.worksap.stm.sb.service;

import java.util.LinkedList;
import java.util.List;

import javax.transaction.Transactional;

import com.worksap.stm.sb.RequirementInfo;
import com.worksap.stm.sb.domain.*;
import com.worksap.stm.sb.type.*;
import org.joda.time.LocalDate;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.worksap.stm.sb.repo.ProposalRepo;
import com.worksap.stm.sb.utils.SbLibrary;

@Service
public class ProposalService {

    @Autowired
    ProposalRepo proposalRepo;

    @Autowired
    RequirementService requirementService;

    @Autowired
    ProductConfigService productConfigService;

    @Autowired
    OpportunityService opportunityService;

    @Autowired
    StatsService statsService;

    @Autowired
    ClientService clientService;


    @Autowired
    TimelineService timelineService;

    @Autowired
    TaskService taskService;

    @Autowired
    ProductRelationService productRelationService;

    @Autowired
    IndustryPerformanceService industryPerformanceService;

    @Transactional
    public JSONObject createNew(long clientid)
    {

        Client client = (Client) clientService.getClientById(clientid).get("data");

        if(client == null)
            return SbLibrary.getResponseObj(false,"Problem Fetching client information");

        List<Requirement> requirements = (List<Requirement>) requirementService.getRequirementsByClient(clientid).get("data");

        if(requirements == null)
            return SbLibrary.getResponseObj(false,"Problem Fetching client information");



        RequirementInfo requirementInfo = requirementService.getLastRequirementInfoByClientid(clientid);


        if(requirementInfo == null)
            return SbLibrary.getResponseObj(false,"No Requirement Information. Please set Requirement info First");

//        System.out.println(requirementInfo);
//        System.out.println(requirements);

        JSONObject cfg = productConfigService.getConfigurations();
//
        float pm = (float) cfg.get("Profit Margin");
        float nightDis = (float)cfg.get("Night Shift Extra");
        float maxDis = (float) cfg.get("Maximum Discount");
        long totalCleaners = (long) cfg.get("Cleaners");
        long perCostCleaner = (long) cfg.get("Per Cost Cleaner");


//        System.out.println(pm);
//        System.out.println(nightDis);
//        System.out.println(maxDis);
//        System.out.println(discount);
//        System.out.println(perCostCleaner);

        Proposal proposal = new Proposal();

        proposal.setClientid(clientid);
        proposal.setOpportunities(new LinkedList<Opportunity>());


        float cost = 0;

        float perDayCost = 0;

        for (Requirement req : requirements)
        {
            Opportunity newOp = opportunityService.create(req,requirementInfo);

            proposal.getOpportunities().add(newOp);

            cost += newOp.getCost();

            perDayCost += newOp.getPerdaycost();
        }

        proposal.setValidfor(requirementInfo.getWeeks());

        proposal.setCost(cost);

        proposal.setPerdaycost(perDayCost);

        long cleaners =  (long) Math.ceil(perDayCost/perCostCleaner);

        proposal.setNumberofcleaners(cleaners);

        long price = (long) (cost+cost*pm);

        proposal.setPrice(price);

        long loyalty = client.getLoyalty();

        System.out.println(loyalty);

        float loyaltyDis = (float) loyalty/100;

        System.out.println(loyaltyDis);

        if(loyaltyDis > maxDis)
        {
            loyaltyDis = maxDis;
        }

        proposal.setLoyaltydis(loyaltyDis);

        proposal.setGivendiscount(0);

        proposal.setDiscount(proposal.getLoyaltydis()+proposal.getGivendiscount());

        System.out.println(proposal.getLoyaltydis()+proposal.getGivendiscount());

        if(proposal.getDiscount() > maxDis)
        {
            proposal.setDiscount(maxDis);
        }

        if(proposal.getDiscount() !=0)
        {
            proposal.setDiscountedprice(proposal.getPrice()-(proposal.getPrice()*proposal.getDiscount()));
        }else
        {
            proposal.setDiscountedprice(proposal.getPrice());
        }



        proposal.setProfit(proposal.getDiscountedprice() - proposal.getCost());

        proposal.setStart(requirementInfo.getStart());

        proposal.setEnd(proposal.getStart().plusWeeks((int) proposal.getValidfor()));

        proposal.setSalesrep(SbLibrary.getCurrentUser().getUser().getUsername());


        Long employedCleaners = proposalRepo.getEmployedCleaners(proposal.getStart());

        if(employedCleaners == null)
            employedCleaners = Long.valueOf(0);

        if((totalCleaners - employedCleaners) < 0 )
        {
            return SbLibrary.getResponseObj(false,"There is not enough available cleaners to provide this service");
        }



        proposal = proposalRepo.save(proposal);


        return SbLibrary.getResponseObj(true,"New Proposal Created",proposal);

    }

    public JSONObject getProposalByClient(Long id) {

        List<Proposal> proposals = proposalRepo.findByClientid(id);

        return SbLibrary.getResponseObj(true, "Proposals", proposals);
    }

    public List<Proposal> getOpenAndUnExpiredProposals(long clientid) {

        return proposalRepo.findOpenAndUnexpiredProposals(clientid);
    }

    @Transactional
    public JSONObject delete(long id) {

        Proposal proposal = proposalRepo.findOne(id);

        if (proposal == null) {
            return SbLibrary.getResponseObj(false, "Proposal Doesn't exists");

        } else if (proposal.isClosed()) {
            return SbLibrary.getResponseObj(false, "Closed Proposal Can't be deleted");
        } else {
            opportunityService.delete(proposal.getOpportunities());

            proposalRepo.delete(proposal.getId());

            return SbLibrary.getResponseObj(true, "Proposal Deleted");
        }

    }

    public Proposal getProposalById(long id) {
        return proposalRepo.findOne(id);
    }

    public Proposal updatePrice(long id, float newPrice) {

        Proposal proposal = proposalRepo.findOne(id);

        proposal.setPrice(newPrice);
        proposal.setProfit(proposal.getPrice() - proposal.getCost());

        proposal.setStatus(ProposalStatus.OPEN);

        return proposalRepo.save(proposal);

    }

    public Proposal updateStatus(long id, ProposalStatus status) {
        Proposal proposal = proposalRepo.findOne(id);

        proposal.setStatus(status);

        if (ProposalStatus.WON.equals(status)) {
            proposal.setClosed(true);

            this.postProposalWin(proposal);

        } else if (ProposalStatus.LOST.equals(status)) {
            proposal.setClosed(true);

            this.postProposalLost(proposal);

        } else if (ProposalStatus.PROPOSED.equals(status)) {
            this.postProposalProposed(proposal);
        }

        taskService.createAutomatedTask(proposal);

        return proposalRepo.save(proposal);
    }

    private void postProposalProposed(Proposal proposal) {

        statsService.createSalesStats(proposal);

        timelineService.create(proposal.getClientid(), "Proposed a Deal", this.getHtmlLink(proposal));

        taskService.setTaskCompleted(proposal.getClientid(),TaskType.PROPOSE);

    }

    private void postProposalLost(Proposal proposal) {

        //statsService.createEventStats(proposal.getSalesrep(), proposal.getClientid(), StatsType.LOST);
        statsService.createSalesStats(proposal);

        timelineService.create(proposal.getClientid(), "Lost the Deal", this.getHtmlLink(proposal));

        taskService.setTaskCompleted(proposal.getClientid(),TaskType.NEGOTIATE);

    }

    private void postProposalWin(Proposal proposal) {
        //statsService.createEventStats(proposal.getSalesrep(), proposal.getClientid(), StatsType.WON);


        JSONObject cfg = productConfigService.getConfigurations();

        long threshold = (long) cfg.get("Loyalty Point Threshold");

        statsService.createSalesStats(proposal);

        productRelationService.updateFromProposal(proposal);

        Client client = (Client) clientService.getClientById(proposal.getClientid()).get("data");

        client.setStatus(ClientStatusType.ACTIVE);

        long loyaltyBonus = (long) Math.floor(proposal.getDiscountedprice()/threshold);


//        System.out.println(loyaltyBonus);

        client.setLoyalty(loyaltyBonus+client.getLoyalty());

        clientService.updateClient(client);

        industryPerformanceService.proposalWon(proposal);

        taskService.setTaskCompleted(proposal.getClientid(), TaskType.NEGOTIATE);

        timelineService.create(proposal.getClientid(), "Won the deal", this.getHtmlLink(proposal));


    }

    public void setExpiredProposals() {

        proposalRepo.setExpireds();

    }


    public String getHtmlLink(Proposal proposal) {
        String url = SbLibrary.getFullUrl("/detailProposal/" + proposal.getId());


        return "<a href=\"" + url + "\" target=_blank>View Proposal</a>";
    }


    public String getHtml(Proposal proposal) {

        StringBuilder proposalStr = new StringBuilder("");
        proposalStr.append("<div class=\"card\">");
        proposalStr.append("<h2>Tasks Details</h2>");
        proposalStr.append("");
        proposalStr.append("<div class=\"table-responsive\">");
        proposalStr.append("<table border=\"3\" class=\"table table-bordered table-stripped\">");
        proposalStr.append("	<thead>");
        proposalStr.append("		<tr>");
        proposalStr.append("			<th>");
        proposalStr.append("			<pre>");
        proposalStr.append("<span style=\"font-size:14px\"><strong><tt>Task Type</tt></strong></span></pre>");
        proposalStr.append("			</th>");
        proposalStr.append("			<th>");
        proposalStr.append("			<pre>");
        proposalStr.append("<span style=\"font-size:14px\"><strong><tt>Area in Sq. m.</tt></strong></span></pre>");
        proposalStr.append("			</th>");
        proposalStr.append("			<th>");
        proposalStr.append("			<pre>");
        proposalStr.append("<span style=\"font-size:14px\"><strong><tt>Days in week</tt></strong></span></pre>");
        proposalStr.append("			</th>");
        proposalStr.append("			<th>");
        proposalStr.append("			<pre>");
        proposalStr.append("<span style=\"font-size:14px\"><strong><tt>Times in a Shift</tt></strong></span></pre>");
        proposalStr.append("			</th>");
        proposalStr.append("			<th>");
        proposalStr.append("			<pre>");
        proposalStr.append("<span style=\"font-size:14px\"><strong><tt> Shift</tt></strong></span></pre>");
        proposalStr.append("			</th>");
        proposalStr.append("		</tr>");
        proposalStr.append("	</thead>");
        proposalStr.append("	<tbody>");

        for (Opportunity op : proposal.getOpportunities()) {

            String shiftStr = "";

            if(op.getShift().isDay() && op.getShift().isNight())
            {
                shiftStr += "Day + Night";
            }else if(op.getShift().isDay())
            {
                shiftStr += "Day";
            }else
            {
                shiftStr += "Night";
            }

            proposalStr.append("		<tr>");
            proposalStr.append("			<td>");
            proposalStr.append("			<pre style=\"text-align:center\">");
            proposalStr.append("<span style=\"font-size:14px\"><strong><tt>" + op.getProduct().getTask() + " " + op.getProduct().getSurface() + " </tt></strong></span></pre>");
            proposalStr.append("			</td>");
            proposalStr.append("			<td>");
            proposalStr.append("			<pre style=\"text-align:center\">");
            proposalStr.append("<span style=\"font-size:14px\"><strong><tt>" + op.getArea() + "</tt></strong></span></pre>");
            proposalStr.append("			</td>");
            proposalStr.append("			<td>");
            proposalStr.append("			<pre style=\"text-align:center\">");
            proposalStr.append("<span style=\"font-size:14px\"><strong><tt>" + op.getWeek() + "</tt></strong></span></pre>");
            proposalStr.append("			</td>");
            proposalStr.append("			<td>");
            proposalStr.append("			<pre style=\"text-align:center\">");
            proposalStr.append("<span style=\"font-size:14px\"><strong><tt>" + op.getTimesinshift() + "</tt></strong></span></pre>");
            proposalStr.append("			</td>");
            proposalStr.append("			<td>");
            proposalStr.append("			<pre style=\"text-align:center\">");
            proposalStr.append("<span style=\"font-size:14px\"><strong><tt>" + shiftStr+ "</tt></strong></span></pre>");
            proposalStr.append("			</td>");
            proposalStr.append("		</tr>");
        }

        proposalStr.append("	</tbody>");
        proposalStr.append("</table>");
        proposalStr.append("</div>");
        proposalStr.append("</div>");
        proposalStr.append("");
        proposalStr.append("<div class=\"card\">");
        proposalStr.append("<h2>Pricing Details</h2>");
        proposalStr.append("");
        proposalStr.append("<div class=\"table-responsive\">");
        proposalStr.append("<table border=\"2\" class=\"table table-bordered\">");
        proposalStr.append("	<tbody>");
        proposalStr.append("		<tr>");
        proposalStr.append("			<td>");
        proposalStr.append("			<pre style=\"text-align:center\">");
        proposalStr.append("<span style=\"font-size:14px\"><tt>Price                 				</tt></span></pre>");
        proposalStr.append("			</td>");
        proposalStr.append("			<td>");
        proposalStr.append("			<pre style=\"text-align:center\">");
        proposalStr.append("<span style=\"font-size:14px\"><tt>S$ " + proposal.getPrice() + "       </tt></span></pre>");
        proposalStr.append("			</td>");
        proposalStr.append("		</tr>");
        proposalStr.append("		<tr>");
        proposalStr.append("			<td>");
        proposalStr.append("			<pre style=\"text-align:center\">");
        proposalStr.append("<span style=\"font-size:14px\"><tt>Loyalty Discount                 	</tt></span></pre>");
        proposalStr.append("			</td>");
        proposalStr.append("			<td>");
        proposalStr.append("			<pre style=\"text-align:center\">");
        proposalStr.append("<span style=\"font-size:14px\"><tt>" + proposal.getLoyaltyDisStr() + "%  </tt></span></pre>");
        proposalStr.append("			</td>");
        proposalStr.append("		</tr>");
        proposalStr.append("		<tr>");
        proposalStr.append("			<td>");
        proposalStr.append("			<pre style=\"text-align:center\">");
        proposalStr.append("<span style=\"font-size:14px\"><tt>Given Discount                 		</tt></span></pre>");
        proposalStr.append("			</td>");
        proposalStr.append("			<td>");
        proposalStr.append("			<pre style=\"text-align:center\">");
        proposalStr.append("<span style=\"font-size:14px\"><tt>" + proposal.getGivenDisStr() + "%   </tt></span></pre>");
        proposalStr.append("			</td>");
        proposalStr.append("		</tr>");
        proposalStr.append("		<tr>");
        proposalStr.append("			<td>");
        proposalStr.append("			<pre style=\"text-align:center\">");
        proposalStr.append("<span style=\"font-size:14px\"><tt>Total Discount                 		</tt></span></pre>");
        proposalStr.append("			</td>");
        proposalStr.append("			<td>");
        proposalStr.append("			<pre style=\"text-align:center\">");
        proposalStr.append("<span style=\"font-size:14px\"><tt> " + proposal.getDisStr() + "%       </tt></span></pre>");
        proposalStr.append("			</td>");
        proposalStr.append("		</tr>");
        proposalStr.append("		<tr>");
        proposalStr.append("			<td>");
        proposalStr.append("			<pre style=\"text-align:center\">");
        proposalStr.append("<span style=\"font-size:14px\"><tt>Discounted Price                 	</tt></span></pre>");
        proposalStr.append("			</td>");
        proposalStr.append("			<td>");
        proposalStr.append("			<pre style=\"text-align:center\">");
        proposalStr.append("<span style=\"font-size:14px\"><tt>S$ " + proposal.getDiscountedprice() + "   </tt></span></pre>");
        proposalStr.append("			</td>");
        proposalStr.append("		</tr>");
        proposalStr.append("	</tbody>");
        proposalStr.append("</table>");
        proposalStr.append("");
        proposalStr.append("<p>&nbsp;</p>");
        proposalStr.append("");
        proposalStr.append("<p><span style=\"font-size:14px\"><small><tt>*All Service Tax and Charges inclusive</tt></small></span></p>");
        proposalStr.append("</div>");
        proposalStr.append("</div>");
        proposalStr.append("");

        return proposalStr.toString();

    }

    public String formattedProposalForMail(long id) {
        Proposal proposal = proposalRepo.findOne(id);

        return getHtml(proposal);
    }

    public Proposal refreshPrices(long id) {

        Proposal proposal = proposalRepo.findOne(id);


//        long validfor = 1;
//        float cost = 0;
//
//        if (proposal != null) {
//
//
//            List<Opportunity> opportunities = proposal.getOpportunities();
//
//            for (Opportunity op : opportunities) {
//                op = opportunityService.refreshOpportunity(op);
//
//                if (validfor < op.getNoofweeks()) {
//                    validfor = op.getNoofweeks();
//                }
//
//                cost += op.getCost();
//            }
//
//            JSONObject cfg = productConfigService.getConfigurations();
//
//            float minpm = (float) cfg.get("MIN_PROFIT_MARGIN");
//            float maxpm = (float) cfg.get("MAX_PROFIT_MARGIN");
//
//
//            proposal.setValidfor(validfor);
//
//            proposal.setCost(cost);
//
//            proposal.setMinprice(cost + minpm * cost);
//            proposal.setMaxprice(cost + maxpm * cost);
//
//            proposal.setPrice((proposal.getMaxprice() + proposal.getMinprice()) / 2);
//
//            proposal.setProfit(proposal.getPrice() - proposal.getCost());
//
//            proposal.setSalesrep(SbLibrary.getCurrentUser().getUser().getUsername());
//
//            proposal = proposalRepo.save(proposal);
//
//        }


        return proposal;
    }

    public List<Proposal> getProposedDeals() {


        List<Proposal> proposals = proposalRepo.findByStatus(ProposalStatus.PROPOSED);
        return proposals;
    }

    public List<Proposal> getLatestProposals() {



        User cuUser = SbLibrary.getCurrentUser().getUser();

        List<Proposal> proposals = null;

        if(cuUser.getRoles().equals("BM"))
        {

            proposals = proposalRepo.findFirstN(15);



        }else if(cuUser.getRoles().equals("SA"))
        {
            proposals = proposalRepo.findSalesrep(cuUser.getUsername(),15);
        }


        return proposals;

    }

    public JSONObject getProposalStats(String username) {

        List<Proposal> proposals = proposalRepo.findSalesrep(username);

        JSONObject retObj = new JSONObject();

        retObj.put("proposals",proposals);
        return retObj;
    }

    public JSONObject getProposalStats() {

        List<Proposal> proposals = (List<Proposal>) proposalRepo.findAll();



        JSONObject retObj = new JSONObject();

        retObj.put("proposals",proposals);
        return retObj;
    }

    public JSONObject removeOpp(long id, long oppid) {

       try {

           opportunityService.delete(oppid);

       }catch (Exception ex)
       {
           return SbLibrary.getResponseObj(false,ex.getMessage());
       }

        Proposal proposal = proposalRepo.findOne(id);

        proposal.setStatus(ProposalStatus.OPEN);

        proposal = this.reCalculate(proposal);

        proposal =  proposalRepo.save(proposal);

        return SbLibrary.getResponseObj(true,"Task Removed",proposal);


    }

    private Proposal reCalculate(Proposal proposal) {


        List<Opportunity>  opportunities = proposal.getOpportunities();

        JSONObject cfg = productConfigService.getConfigurations();

        float pm = (float) cfg.get("Profit Margin");
        float nightDis = (float)cfg.get("Night Shift Extra");
        float maxDis = (float) cfg.get("Maximum Discount");
        long discount = (long) cfg.get("Cleaners");
        long perCostCleaner = (long) cfg.get("Per Cost Cleaner");


        float cost = 0;

        float perDayCost = 0;


        for (Opportunity op : opportunities)
        {
            cost += op.getCost();

            perDayCost += op.getPerdaycost();
        }

        proposal.setCost(cost);

        proposal.setPerdaycost(perDayCost);

        long cleaners =  (long) Math.ceil(perDayCost/perCostCleaner);

        proposal.setNumberofcleaners(cleaners);

        long price = (long) (cost+cost*pm);

        proposal.setPrice(price);


        proposal.setDiscount(proposal.getLoyaltydis()+proposal.getGivendiscount());

        if(proposal.getDiscount() > maxDis)
        {
            proposal.setDiscount(maxDis);
            proposal.setGivendiscount(maxDis-proposal.getLoyaltydis());
        }

        if(proposal.getDiscount() !=0)
        {
            proposal.setDiscountedprice(proposal.getPrice()-(proposal.getPrice()*proposal.getDiscount()));

        }else
        {
            proposal.setDiscountedprice(proposal.getPrice());
        }


        proposal.setProfit(proposal.getDiscountedprice() - proposal.getCost());


        proposal.setSalesrep(SbLibrary.getCurrentUser().getUser().getUsername());


        return  proposal;

    }

    public List<Proposal> getWonDeals() {

        return proposalRepo.findByStatus(ProposalStatus.WON);
    }


    public JSONObject updateProposal(long id, LocalDate start, long duration, float discount, long cleaners)
    {


        Proposal proposal = this.proposalRepo.findOne(id);


        if(proposal != null)
        {
            if(duration != 0)
            {
                proposal.setValidfor(duration);
            }

            System.out.println(proposal);

            for(Opportunity op:proposal.getOpportunities())
            {
                 op = opportunityService.updateOpp(op,duration);
            }


            proposal.setGivendiscount(discount);

            proposal = reCalculate(proposal);

            System.out.println(proposal);

            proposal.setStart(start);
            proposal.setEnd(proposal.getStart().plusWeeks((int) proposal.getValidfor()));


            if(cleaners >0)
            {
                proposal.setNumberofcleaners(cleaners);
            }


            proposal.setStatus(ProposalStatus.OPEN);

            proposal = proposalRepo.save(proposal);


            JSONObject cfg = productConfigService.getConfigurations();

            float maxDis = (float) cfg.get("Maximum Discount");

            if(discount + proposal.getLoyaltydis() > maxDis)
            {
                return SbLibrary.getResponseObj(true,"Proposal Updated. You can not give discount mroe than maximum discount set by the manager",proposal);
            }


            return SbLibrary.getResponseObj(true,"Proposal Updated",proposal);
        }


        return SbLibrary.getResponseObj(false,"Proposal Does nto exists");
    }
}
