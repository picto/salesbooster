package com.worksap.stm.sb;


import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

import com.worksap.stm.sb.utils.PasswordHash;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsService userDetailsService;
	


//	RequestMatcher csrfRequestMatcher = new RequestMatcher() {
//
//		private String allowedMethod = "GET";
//
//		private AntPathRequestMatcher[] requestMatchers = {
//				new AntPathRequestMatcher("/login"),
//				new AntPathRequestMatcher("/logout"),
//			 };
//
//		@Override
//		public boolean matches(HttpServletRequest request) {
//			for (AntPathRequestMatcher rm : requestMatchers) {
//				if (rm.matches(request)) {
//					return false;
//				}
//			}
//			if (request.getMethod().equals(allowedMethod)) {
//				return false;
//			}
//			return true;
//		}
//
//	};

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.csrf()
				.disable();
//				.requireCsrfProtectionMatcher(csrfRequestMatcher);
		
		http
			.authorizeRequests()
			.antMatchers("/index/**","/resources/**", "/login/**","/contactUs")
			.permitAll()
			.anyRequest()
			.authenticated();
		
		http
			.formLogin()
			.loginPage("/login")
			.failureUrl("/login")
			.usernameParameter("username")
			.permitAll();
		
		http
			.logout()
			.logoutUrl("/logout").logoutSuccessUrl("/login").permitAll();
		
		


	}

	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth
		.userDetailsService(userDetailsService)
		.passwordEncoder(new PasswordHash());
	}
}