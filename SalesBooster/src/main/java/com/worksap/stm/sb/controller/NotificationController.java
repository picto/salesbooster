package com.worksap.stm.sb.controller;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.worksap.stm.sb.domain.Notification;
import com.worksap.stm.sb.service.NotificationService;
import com.worksap.stm.sb.utils.SbLibrary;

@Controller
public class NotificationController
{

	@Autowired
	NotificationService notificationService;

	@RequestMapping(value = "/notifications", method = RequestMethod.GET)
	public String allNotifications(HttpServletRequest request, Model model)
	{

		JSONObject obj = notificationService.getNotifications();

		model.addAttribute("success", obj.get("success"));
		model.addAttribute("msg", obj.get("msg"));
		model.addAttribute("data", obj.get("data"));

		return SbLibrary.getPageFragment(request, "notifications");
	}

	@RequestMapping(value = "/removeNotification", method = RequestMethod.POST)
	public @ResponseBody JSONObject setSeen(@RequestBody JSONObject data, HttpServletRequest request, Model model)
	{
		int id = (int) data.get("id");

		return notificationService.remove(id);
	}

	@RequestMapping(value = "/countUnseenNotifications", method = RequestMethod.GET)
	public @ResponseBody JSONObject setSeen(HttpServletRequest request, Model model)
	{

		String username = SbLibrary.getCurrentUser().getUsername();

		return notificationService.countUnseenNotifications(username);
	}

	@RequestMapping(value = "/visitNotificationLink/{id}", method = RequestMethod.GET)
	public String visitNotificationLink(@PathVariable("id") long id) throws Exception
	{

		Notification notification = notificationService.setNotificationAsSeen(id);

		if (notification.getLink() != null)
		{
			return SbLibrary.getRedirectUrl(notification.getLink(), "");

		} else
		{
			return SbLibrary.getRedirectUrl("/getClientDetails/", notification.getClientid());
		}

	}
	@RequestMapping(value = "/setNotificationShown/{id}", method = RequestMethod.GET)
	public @ResponseBody void setNotificationShown(@PathVariable("id") long id) throws Exception
	{
		
		notificationService.setNotificationAsShown(id);
	
	}
	@RequestMapping(value = "/getUnshowedNotifications", method = RequestMethod.GET)
	public @ResponseBody JSONObject getUnshowedNotifications() throws Exception
	{
		
		return notificationService.getUnshowedNotifications();
		
	}
	
	@RequestMapping(value = "/getSingleUnshowedNotifications", method = RequestMethod.GET)
	public @ResponseBody JSONObject getSingleUnshowedNotifications() throws Exception
	{
		
		return notificationService.getSingleUnshowedNotifications();
		
	}

}
