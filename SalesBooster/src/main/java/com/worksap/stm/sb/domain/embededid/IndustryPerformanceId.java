package com.worksap.stm.sb.domain.embededid;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by mamun on 13/1/2016.
 */


@NoArgsConstructor
@AllArgsConstructor
@Data
@Embeddable

public class IndustryPerformanceId implements Serializable {


    private String username;
    private String industry;
}
