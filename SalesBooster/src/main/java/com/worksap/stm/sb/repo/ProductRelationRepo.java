package com.worksap.stm.sb.repo;


import com.worksap.stm.sb.domain.ProductRelation;
import com.worksap.stm.sb.domain.embededid.ProductRelationId;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

/**
 * Created by mamun on 8/1/2016.
 */
public interface ProductRelationRepo extends PagingAndSortingRepository<ProductRelation,ProductRelationId>{

    @Query(value = "SELECT * FROM productrelation p WHERE p.productid = ?1 ORDER BY p.times DESC LIMIT ?2 ",nativeQuery = true)
    List<ProductRelation> findTopRelatedProducts(long id,int limit);
}
