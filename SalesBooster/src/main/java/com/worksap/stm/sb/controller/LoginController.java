package com.worksap.stm.sb.controller;

import com.worksap.stm.sb.domain.Client;
import com.worksap.stm.sb.service.ClientService;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LoginController {

    @Autowired
    ClientService clientService;

	@RequestMapping(value = "/login")
	public String login() {
		return "login";
	}



	@RequestMapping(value = "/contactUs")
	public String contactUs() {
		return "contact-us";
	}

    @RequestMapping(value = "/contactUs",method = RequestMethod.POST)
	public String contactUsPost(@ModelAttribute Client client, BindingResult bindingResult) {


//        System.out.println(client);
//
        if(!bindingResult.hasErrors())
        {
            JSONObject addObj = clientService.addContactUs(client);

            if((boolean)addObj.get("success"))
            {
                return "thank-you";
            }
        }




        return "contact-us";

    }

}
