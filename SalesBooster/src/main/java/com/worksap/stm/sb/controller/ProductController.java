package com.worksap.stm.sb.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.worksap.stm.sb.service.StatsService;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.worksap.stm.sb.domain.Product;
import com.worksap.stm.sb.service.NotificationService;
import com.worksap.stm.sb.service.ProductService;
import com.worksap.stm.sb.utils.SbLibrary;

@Controller
public class ProductController
{

	@Autowired
	ProductService productService;
	
	
	@Autowired
	NotificationService notificationService;


	@Autowired
	StatsService statsService;

	@RequestMapping(value = "/getSurfaceTypes", method = RequestMethod.GET)
	public @ResponseBody List<String> getCategories() throws Exception
	{
		return productService.getDistinctCategories();
	}

	@RequestMapping(value = "/getTasks/{surface}", method = RequestMethod.GET)
	public @ResponseBody List<String> getTasks(@PathVariable("surface") String surface) throws Exception
	{
		return productService.getTasks(surface);
	}

	@RequestMapping(value = "/createService", method = RequestMethod.GET)
	public String getCreateService(HttpServletRequest request)
	{
		return SbLibrary.getPageFragment(request, "create-service");
	}

	@RequestMapping(value = "/createService", method = RequestMethod.POST)
	public String postCreateService(@ModelAttribute Product product, HttpServletRequest request, BindingResult bindingResult)
	{

		if (bindingResult.hasErrors())
		{
			return SbLibrary.getPageFragment(request, "create-service");

		} else
		{

		}

		System.err.println(product);

		productService.createProduct(product);

		return SbLibrary.getRedirectUrl("/serviceManagement", "");

	}

	@RequestMapping(value = "/serviceManagement", method = RequestMethod.GET)
	public String serviceManagement(HttpServletRequest request, Model model)
	{

		JSONObject obj = productService.getAllServices();

		model.addAttribute("success", obj.get("success"));
		model.addAttribute("msg", obj.get("msg"));

		if ((boolean) obj.get("success"))
		{
			model.addAttribute("data", obj.get("data"));
		}

		return SbLibrary.getPageFragment(request, "service-management");

	}

	@RequestMapping(value = "/deleteService", method = RequestMethod.POST)
	public @ResponseBody JSONObject deleteService(@RequestBody JSONObject postData, HttpServletRequest request, Model model,
			BindingResult bindingResult)
	{

		JSONObject retObj = null;
		
		if (bindingResult.hasErrors())
		{
			retObj.put("success", false);
			retObj.put("msg", "Failed to Delete");

		} else
		{
			// Create in DB and return

//			System.err.println(postData);

			String idStr = (String) postData.get("id");

			long id = Long.parseLong(idStr);

			retObj = productService.deleteService(id);

		}

		return retObj;
	}
	
	
	@RequestMapping(value = "/editService/{id}", method = RequestMethod.POST)
	public @ResponseBody JSONObject deleteService(@PathVariable("id") long id,@RequestBody Product product, HttpServletRequest request, Model model,
			BindingResult bindingResult)
	{
		
		JSONObject retObj = null;
		
		if (bindingResult.hasErrors())
		{
			return SbLibrary.getResponseObj(false, "Binding error");
			
		} else
		{
			// Create in DB and return
			
//			System.err.println(product);
			
			
			retObj =  productService.editProduct(product);
			
			
//			if((boolean) retObj.get("success"))
//			{
//				notificationService.notifyAllSalesRep("Prices Changed!","There has been some changes made in service prices.Please Refresh all Open proposals",0,"");
//			}

			return retObj;
		}
		

	}




	@RequestMapping(value = "/getRevenueByProduct", method = RequestMethod.GET)
	public @ResponseBody JSONObject getRevenueByProduct(HttpServletRequest request, Model model)
	{

		JSONObject obj = statsService.getRevenueByProduct();

		return obj;

	}
	
	

}
