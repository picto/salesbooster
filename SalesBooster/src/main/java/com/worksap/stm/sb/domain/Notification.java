package com.worksap.stm.sb.domain;


import javax.persistence.Entity;
import org.hibernate.validator.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper=true)
@Data
@Entity
public class Notification extends BaseEntity
{
	
	
	@NotBlank
	private String title;
	
	@NotBlank
	private String msg;
	
	private String link;
	
	private boolean seen = false;
	private boolean shown = false;
	
	
	
	private long clientid;
	
	private String srepresentative;

}
