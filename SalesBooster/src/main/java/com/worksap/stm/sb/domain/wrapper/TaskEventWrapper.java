package com.worksap.stm.sb.domain.wrapper;



import com.worksap.stm.sb.domain.Task;
import com.worksap.stm.sb.type.ClientStatusType;
import com.worksap.stm.sb.type.TaskStatus;
import com.worksap.stm.sb.utils.SbLibrary;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class TaskEventWrapper
{

	
	private long id;
	
	private String title;
	
	private String type;
	
	private String start;
	
	private String end;
	
	private long clientid;
	
	private ClientStatusType clientstatus;
	
	private String status;
	
	private String color;
	
	private String srepresentative;
	
	private String url;
	
	private boolean editable = true;
	private boolean teammeeting = false;

	

	
	
	public TaskEventWrapper(Task task)
	{
		this.id = task.getId();

		this.type = task.getType().toString();

		this.start = task.getStart().toString();
		
		this.end = task.getEnd().toString();
		
		this.clientid = task.getClientid();
		
		this.clientstatus = task.getClientstatus();
		
		this.status = task.getStatus().toString();

		if(task.isTeammeeting())
		{

			this.title =  "Team Meeting "+task.getTitle();

			this.color =  (task.getStatus().equals(TaskStatus.COMPLETED)? "#260C26":"#59A559");

		}else
		{
			this.title = task.getTitle();

			this.color = task.getStatus().equals(TaskStatus.DUE) ? "#7DBFF1":"#7F4CB2";

		}

		this.color = task.isMissed()? "#EE2940":this.color;
		
		this.srepresentative = task.getSrepresentative();
		
		this.url = task.isTeammeeting() ? "":SbLibrary.getFullUrl("/getClientDetails/"+ task.getClientid());

		this.editable = (!task.isTeammeeting() && !task.isMissed());


		this.teammeeting = task.isTeammeeting()? true:false;
	}
	
	
}
