package com.worksap.stm.sb.type;

public enum TaskStatus
{

	DUE,
	COMPLETED
}
