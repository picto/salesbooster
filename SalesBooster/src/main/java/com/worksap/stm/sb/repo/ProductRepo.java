package com.worksap.stm.sb.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.worksap.stm.sb.domain.Product;

public interface ProductRepo extends PagingAndSortingRepository<Product, Long>
{

	
	@Query(value="SELECT DISTINCT surface FROM product p",nativeQuery=true)
	List<String> findDistinctSurfaces();
	
	@Query(value="SELECT task FROM product p WHERE p.surface = ?1",nativeQuery=true)
	List<String> findTasksBySurface(String category);

	Product findBySurfaceAndTask(String cat, String task);
	
	List<Product> findBySurface(String surface);

	List<Product> findByTagsContaining(String tag);
	
	
	
}
