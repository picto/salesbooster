package com.worksap.stm.sb.repo;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.worksap.stm.sb.domain.Client;
import com.worksap.stm.sb.domain.Requirement;

public interface RequirementRepo extends PagingAndSortingRepository<Requirement, Long>
{

	List<Requirement> findByClient(Client client);

}
