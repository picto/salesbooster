package com.worksap.stm.sb.interceptor;

import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.worksap.stm.sb.domain.User;
import com.worksap.stm.sb.entity.CurrentUser;

@Component
public class AccessControlInterceptor extends HandlerInterceptorAdapter {


	@Override
	public boolean preHandle(HttpServletRequest request,HttpServletResponse response, Object handler) throws Exception {

		if (request.getUserPrincipal() != null) {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			if (request.getSession().getAttribute("roles") == null) 
			{
				request.getSession().setAttribute("roles", auth.getAuthorities().stream()
						.map(GrantedAuthority::getAuthority).collect(Collectors.toList()));	
				
			
				CurrentUser currentUser = (CurrentUser) auth.getPrincipal();
				
				HttpSession session = request.getSession();
				
				User cu = currentUser.getUser();
				
				session.setAttribute("username", cu.getUsername());
				session.setAttribute("fullname", cu.getName());
				session.setAttribute("email", cu.getEmail());
				session.setAttribute("email_pass", cu.getEmailPass());
				
			}
			if (request.getRequestURI().toString().endsWith("login")) {
				response.sendRedirect(request.getContextPath() + "/");
				return false;
			}
			
		}

		return true;
	}
}