package com.worksap.stm.sb.controller;

import javax.servlet.http.HttpServletRequest;

import com.worksap.stm.sb.domain.User;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.worksap.stm.sb.domain.Client;
import com.worksap.stm.sb.domain.Task;
import com.worksap.stm.sb.service.ClientService;
import com.worksap.stm.sb.service.TaskService;
import com.worksap.stm.sb.type.ClientStatusType;
import com.worksap.stm.sb.type.TaskStatus;
import com.worksap.stm.sb.type.TaskType;
import com.worksap.stm.sb.utils.SbLibrary;

@Controller
public class TaskController
{

	@Autowired
	TaskService taskService;
	
	
	@Autowired
	ClientService clientService;


	@RequestMapping(value = "/showCalendar")
	public String showCalendar(HttpServletRequest request, Model model) throws Exception
	{

		User user = SbLibrary.getCurrentUser().getUser();

		if(user.getRoles().equals("BM"))
		{
			model.addAttribute("tasks", taskService.getAllTaskForManager());

			return SbLibrary.getPageFragment(request,"calendar-manager");
		}else
		{
			model.addAttribute("tasks", taskService.getTaskByUser(user.getUsername()));

			return SbLibrary.getPageFragment(request,"calendar");
		}
	}

	@ResponseBody
	@RequestMapping(value = "/createTask", method = RequestMethod.POST)
	public JSONObject createTask(@RequestBody JSONObject postData, HttpServletRequest request)
	{

		Task task = new Task();
		task.setTitle((String) postData.get("title"));
		task.setType(TaskType.valueOf((String) postData.get("type")));
		task.setStart(LocalDateTime.parse((String) postData.get("start"), DateTimeFormat.forPattern("dd/MM/yyyy HH:mm")));
		task.setEnd(LocalDateTime.parse((String) postData.get("end"), DateTimeFormat.forPattern("dd/MM/yyyy HH:mm")));
		task.setSrepresentative(SbLibrary.getCurrentUser().getUsername());
		task.setClientid(Long.parseLong((String) postData.get("clientid")));
		task.setClientstatus(ClientStatusType.valueOf((String) postData.get("clientstatus")));

		return taskService.create(task);
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/createTaskCal", method = RequestMethod.POST)
	public JSONObject createTaskCal(@RequestBody JSONObject postData, HttpServletRequest request)
	{
		
		Task task = new Task();
		task.setTitle((String) postData.get("title"));
		task.setType(TaskType.valueOf((String) postData.get("type")));
		task.setStart(LocalDateTime.parse((String) postData.get("start"), DateTimeFormat.forPattern("dd/MM/yyyy-HH:mm")));
		task.setEnd(LocalDateTime.parse((String) postData.get("end"), DateTimeFormat.forPattern("dd/MM/yyyy-HH:mm")));
		task.setSrepresentative(SbLibrary.getCurrentUser().getUsername());
		task.setClientid(Long.parseLong((String) postData.get("clientid")));

		if(postData.containsKey("fixed"))
		{
			task.setTeammeeting(true);
		}



		if(!task.isTeammeeting() && task.getClientid() >0) {

			JSONObject obj = clientService.getClientById(task.getClientid());

			if ((boolean) obj.get("success")) {
				Client client = (Client) obj.get("data");

				task.setClientstatus(client.getStatus());
			} else {
				return SbLibrary.getResponseObj(false, "Couldn't Fetch client information. Try again");
			}
		}else
		{

		}
		
		return taskService.create(task);
	}

	@RequestMapping(value = "/editTask/{id}", method = RequestMethod.POST)
	public @ResponseBody JSONObject editTask(@RequestBody JSONObject postData, @PathVariable("id") long id, HttpServletRequest request, Model model,
			BindingResult bindingResult)
	{

		Task task = taskService.getTaskById(id);
//		task.setId(id);
		task.setTitle((String) postData.get("title"));
		task.setType(TaskType.valueOf((String) postData.get("type")));
		task.setStart(LocalDateTime.parse((String) postData.get("start"), DateTimeFormat.forPattern("dd/MM/yyyy HH:mm")));
		task.setEnd(LocalDateTime.parse((String) postData.get("end"), DateTimeFormat.forPattern("dd/MM/yyyy HH:mm")));
//		task.setSrepresentative((String) postData.get("srepresentative"));
//		task.setClientid(Long.parseLong(postData.get("clientid").toString()));

//		task.setClientstatus(ClientStatusType.valueOf((String) postData.get("clientstatus")));

		if (postData.containsKey("completed")) {
			if (postData.get("completed").equals("on"))
				task.setStatus(TaskStatus.COMPLETED);
			else
			{
				task.setStatus(TaskStatus.DUE);
			}
		}

//		System.out.println(postData);
//		System.out.println(task);
		return taskService.editTask(task);
	}

	@RequestMapping(value = "/editTaskCal/{id}", method = RequestMethod.POST)
	public @ResponseBody JSONObject editTaskCal(@RequestBody JSONObject postData, @PathVariable("id") long id, HttpServletRequest request,
			Model model, BindingResult bindingResult)
	{

		Task task = taskService.getTaskById(id);
		task.setId(id);
//		task.setTitle((String) postData.get("title"));
//		task.setType(TaskType.valueOf((String) postData.get("type")));
		task.setStart(LocalDateTime.parse((String) postData.get("start"), DateTimeFormat.forPattern("dd/MM/yyyy-HH:mm")));
		task.setEnd(LocalDateTime.parse((String) postData.get("end"), DateTimeFormat.forPattern("dd/MM/yyyy-HH:mm")));
//		task.setSrepresentative((String) postData.get("srepresentative"));
//		task.setClientid(Long.parseLong(postData.get("clientid").toString()));
//		task.setStatus(TaskStatus.valueOf((String) postData.get("status")));

//		if(postData.get("clientstatus") != null)
//			task.setClientstatus(ClientStatusType.valueOf((String) postData.get("clientstatus")));


//		task.setTeammeeting((Boolean) postData.get("fixed"));

		return taskService.editTask(task);
	}

	@RequestMapping(value = "/deleteTask", method = RequestMethod.POST)
	public @ResponseBody JSONObject deleteTask(@RequestBody JSONObject postData, HttpServletRequest request, Model model, BindingResult bindingResult)
	{

		JSONObject retObj = new JSONObject();

		if (bindingResult.hasErrors())
		{
			retObj.put("success", false);
			retObj.put("msg", "Failed to Delete");

		} else
		{
			// Create in DB and return


			String idStr = (String) postData.get("id");

			long id = Long.parseLong(idStr);

			retObj = taskService.delete(id);

		}

		return retObj;

	}

	@RequestMapping(value = "/noOfTaskToday", method = RequestMethod.GET)
	public @ResponseBody JSONObject noOfTaskToday()
	{
		return taskService.countDueTaskToday();
	}
}
