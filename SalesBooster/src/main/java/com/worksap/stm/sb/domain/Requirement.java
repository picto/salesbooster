package com.worksap.stm.sb.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;


import com.worksap.stm.sb.domain.embedded.Week;
import com.worksap.stm.sb.domain.embedded.WorkingShift;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class Requirement extends BaseEntity
{

    @NotNull
    @ManyToOne(cascade=CascadeType.DETACH)
    @JoinColumn(name = "client", insertable = true, updatable = true)
    private Client client;


    @NotNull
    @Embedded
    private Week week;

    @NotNull
    private int timesinshift;

    @NotNull
    @Embedded
    private WorkingShift shift;


    @NotNull
    private float area;

    @NotNull
    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "product", insertable = true, updatable = true)
    private Product product;

    private boolean required;




}
