package com.worksap.stm.sb.repo;

import com.worksap.stm.sb.RequirementInfo;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

/**
 * Created by mamun on 14/1/2016.
 */



public interface RequirementInfoRepo extends PagingAndSortingRepository<RequirementInfo,Long>
{

    List<RequirementInfo> findByClientid(Long id);


    @Query(value = "SELECT * from requirementinfo r where r.clientid = ?1 order by r.created desc limit 1",nativeQuery = true)
    RequirementInfo findFirstByClientidOrderByCreated(long clientid);
}
