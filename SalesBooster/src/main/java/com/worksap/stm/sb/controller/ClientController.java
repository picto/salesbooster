package com.worksap.stm.sb.controller;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_LIGHTENPeer;
import com.worksap.stm.sb.domain.IndustryPerformance;
import com.worksap.stm.sb.service.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringArrayPropertyEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.worksap.stm.sb.domain.Client;
import com.worksap.stm.sb.domain.User;
import com.worksap.stm.sb.type.ClientStatusType;
import com.worksap.stm.sb.utils.SbLibrary;

@Controller
public class ClientController
{

	@Autowired
	ClientService clientService;

	@Autowired
	UserService userService;

	@Autowired
	NoteService notesService;

	@Autowired
	TimelineService timelineService;

	@Autowired
	TaskService taskService;

	@Autowired
	RequirementService requirementService;
	
	@Autowired
	ProposalService proposalService;
	
	@Autowired
	EmailService emailService;

	@Autowired
    StatsService statsService;

    @Autowired
    IndustryPerformanceService industryPerformanceService;

	@Autowired
	NotificationService notificationService;

//	@RequestMapping(value = "/singleSuspectSave", method = RequestMethod.POST, consumes = "application/json")
//	public @ResponseBody JSONObject saveSingleSuspect(@RequestBody Client postData, HttpServletRequest request, Model model)
//	{
//		return clientService.addSuspect(postData);
//	}

//	@RequestMapping(value = "/createSuspect", method = RequestMethod.GET)
//	public String createNewSuspectPage( HttpServletRequest request, Model model)
//	{
//
//		return SbLibrary.getPageFragment(request, "create-client");
//
//	}
//
//	@RequestMapping(value = "/createSuspect", method = RequestMethod.POST)
//	public String createNewSuspect(@ModelAttribute Client postData, HttpServletRequest request, Model model)
//	{
//		JSONObject obj = null;
//
//		obj = clientService.addSuspect(postData);
//
//		if ((boolean) obj.get("success"))
//		{
//			Client newClient = (Client) obj.get("data");
//
//			return SbLibrary.getRedirectUrl("/getClientDetails/", newClient.getId());
//
//		} else
//		{
//			model.addAttribute("success", obj.get("success"));
//			model.addAttribute("msg", obj.get("msg"));
//			model.addAttribute("data", postData);
//
//			return SbLibrary.getPageFragment(request, "create-client");
//		}
//	}

//	@RequestMapping(value = "/unassignedSuspects", method = RequestMethod.GET)
//	public String getUnassignedSuspects(HttpServletRequest request, Model model)
//	{
//
//		JSONObject obj = clientService.getAllUnassigenedSuspect();
//
////        System.out.println(obj);
//
//		List<User> allSalesAssocs = userService.getAllSalesAssocs();
//
//        JSONArray saArr = new JSONArray();
//
//        for(User user:allSalesAssocs)
//        {
//            JSONObject userObj = new JSONObject();
//            userObj.put("user",user);
//            userObj.put("stats",statsService.getStats(user.getUsername()));
//
//            saArr.add(userObj);
//        }
//
//
//        model.addAttribute("success", obj.get("success"));
//        model.addAttribute("msg", obj.get("msg"));
//        model.addAttribute("data", obj.get("data"));
//        model.addAttribute("sa", saArr);
//
//
//        return SbLibrary.getPageFragment(request, "unassigned-suspects");
//
//	}


	@RequestMapping(value = "/unassignedProspects", method = RequestMethod.GET)
	public String getUnassignedProspects(HttpServletRequest request, Model model)
	{

		JSONObject obj = clientService.getAllUnassigenedProspects();

		List<User> allSalesAssocs = userService.getAllSalesAssocs();


        JSONArray saArr = new JSONArray();
        for(User user:allSalesAssocs)
        {
           JSONObject userObj = new JSONObject();
            userObj.put("user",user);
            userObj.put("stats",statsService.getStats(user.getUsername()));

            saArr.add(userObj);
        }

        model.addAttribute("success", obj.get("success"));
		model.addAttribute("msg", obj.get("msg"));
		model.addAttribute("data", obj.get("data"));

		model.addAttribute("sa", saArr);

		return SbLibrary.getPageFragment(request, "unassigned-clients");

	}

	@RequestMapping(value = "/assignedClients", method = RequestMethod.GET)
	public String getAssignedClients(HttpServletRequest request, Model model)
	{

		JSONObject obj = clientService.getAllAssignedClients();

		model.addAttribute("success", obj.get("success"));
		model.addAttribute("msg", obj.get("msg"));
		model.addAttribute("data", obj.get("data"));

		return SbLibrary.getPageFragment(request, "assigned-clients");
	}

	// returns with UI
	@RequestMapping(value = "/myClients", method = RequestMethod.GET)
	public String getMyClients(HttpServletRequest request, Model model)
	{

		JSONObject obj = clientService.getMyClients();

		model.addAttribute("success", obj.get("success"));
		model.addAttribute("msg", obj.get("msg"));
		model.addAttribute("data", obj.get("data"));


        model.addAttribute("page","All");
		return SbLibrary.getPageFragment(request, "my-clients");

	}
	// returns with UI
	@RequestMapping(value = "/myClients/{status}", method = RequestMethod.GET)
	public String getMyClientsSelective(@PathVariable ClientStatusType status, HttpServletRequest request, Model model)
	{
		JSONObject obj  = clientService.getMyClientsByStatus(status);
		
		model.addAttribute("success", obj.get("success"));
		model.addAttribute("msg", obj.get("msg"));
		model.addAttribute("data", obj.get("data"));

        model.addAttribute("page",status);
		
		return SbLibrary.getPageFragment(request, "my-clients");
		
	}

	// Returns as a json list
	@RequestMapping(value = "/myClientsList", method = RequestMethod.GET)
	public @ResponseBody JSONObject getMyClientsJson(HttpServletRequest request, Model model)
	{
		JSONObject obj = clientService.getMyClients();
		return obj;
	}



	@RequestMapping(value = "/getClientDetails/{clientid}", method = RequestMethod.GET)
	public String getClientDetails(@PathVariable("clientid") Long id, HttpServletRequest request, Model model)
	{


		JSONObject obj = clientService.getClientById(id);


		Client client = (Client) obj.get("data");

		User curUser = SbLibrary.getCurrentUser().getUser();

		if(!curUser.getRoles().equals("BM") &&
				(!curUser.getUsername().equals(client.getSrepresentative()) && !curUser.getUsername().equals(client.getSadder())) &&
					!client.isDelegated())
		{
			return SbLibrary.getRedirectUrl("/myPipeline","");
		}

		model.addAttribute("success", obj.get("success"));
		model.addAttribute("msg", obj.get("msg"));
		model.addAttribute("clientid",id);
		if (obj.get("data") != null)
		{
			model.addAttribute("data", obj.get("data"));

		}

		JSONObject notesObj = notesService.getClientNotes(id);

		if (notesObj.get("data") != null)
		{
			model.addAttribute("notes", notesObj.get("data"));

		}

		JSONObject tEventsObj = timelineService.getTimelineEvents(id);

		if (tEventsObj.get("data") != null)
		{
			model.addAttribute("tEvents", tEventsObj.get("data"));

		}

		JSONObject taskObject = taskService.getTasksByClient(id);

		if (taskObject.get("data") != null)
		{
			model.addAttribute("tasks", taskObject.get("data"));

		}

		JSONObject reqObj = requirementService.getRequirementsByClient(id);

		
		if (reqObj.get("data") != null)
		{
			model.addAttribute("requirements", reqObj.get("data"));

		}

		JSONObject reqInfoObj = requirementService.getRequirementsInfoByClient(id);


		if (reqInfoObj.get("data") != null)
		{
			model.addAttribute("reqInfos", reqInfoObj.get("data"));

		}
		
		
		
		JSONObject propObj = proposalService.getProposalByClient(id);
		
		
		if (propObj.get("data") != null)
		{
			model.addAttribute("proposals", propObj.get("data"));
			
		}
		
		JSONObject emailSentObj = emailService.getSentEmailByCLientid(id);
		
		
		if (emailSentObj.get("data") != null)
		{
			model.addAttribute("sentmails", emailSentObj.get("data"));
			
		}
		
		

		return SbLibrary.getPageFragment(request, "client-details");

	}

	@RequestMapping(value = "/editClient/{clientid}", method = RequestMethod.GET)
	public String getEditForm(@PathVariable("clientid") Long id, HttpServletRequest request, Model model)
	{

		JSONObject obj = clientService.getClientById(id);

		List<User> salesAccos = userService.getAllSalesAssocs();

		model.addAttribute("success", obj.get("success"));
		model.addAttribute("msg", obj.get("msg"));

		if (obj.get("data") != null)
			model.addAttribute("data", obj.get("data"));

		model.addAttribute("sa", salesAccos);
		
		model.addAttribute("backurl", SbLibrary.getUrl("/getClientDetails/", id));

		return SbLibrary.getPageFragment(request, "edit-client");

	}

	@RequestMapping(value = "/editClient/{clientid}", method = RequestMethod.POST)
	public String postEdit(@PathVariable("clientid") Long clientid, @ModelAttribute Client client, HttpServletRequest request, Model model,
			BindingResult bindingResult)
	{

		if (bindingResult.hasErrors())
		{
			// handle errors and return

			JSONObject obj = clientService.getClientById(clientid);
			List<User> salesAccos = userService.getAllSalesAssocs();

			obj.put("msg", "Error in bind result. Make Sure you posted corretly");

			model.addAttribute("success", obj.get("success"));
			model.addAttribute("msg", obj.get("msg"));
			if (obj.get("data") != null)
				model.addAttribute("data", obj.get("data"));

			model.addAttribute("sa", salesAccos);
			
			model.addAttribute("backurl", SbLibrary.getUrl("/getClientDetails/", clientid));

			return SbLibrary.getPageFragment(request, "edit-client");

		} else
		{
			// Create in DB and return

			clientService.updateClient(client);
			
			model.addAttribute("backurl", SbLibrary.getUrl("/getClientDetails/", clientid));

			return SbLibrary.getRedirectUrl("/getClientDetails/", clientid);
		}

	}



    @PreAuthorize("hasRole('BM')")
	@RequestMapping(value = "/assignSalesRep/{salesrep}/{clientid}", method = RequestMethod.GET)
	public @ResponseBody JSONObject assignSalesRep(@PathVariable("salesrep") String salesrep,@PathVariable("clientid") long clientid,HttpServletRequest request, Model model)
	{

		Client client =(Client) clientService.getClientById(clientid).get("data");

		List<IndustryPerformance> industryPerformances = industryPerformanceService.getPerformanceByIndustry(client.getIndustry());

		List<User> allSalesAssocs = userService.getAllSalesAssocs();

		JSONArray saArr = new JSONArray();

		for(User user:allSalesAssocs)
		{
			if(user.getUsername().equals(salesrep)) continue;

			JSONObject userObj = new JSONObject();
			userObj.put("empty","");
			userObj.put("user",user);
			userObj.put("clients",clientService.getNumberOfClients(user.getUsername()));
			userObj.put("stats",statsService.getStats(user.getUsername()));

			userObj.put("industryAmount",0+" amount won for "+client.getIndustry());
			userObj.put("industryDeals",0+" deals won for "+client.getIndustry());

//            System.out.println(industryPerformances);

			for( IndustryPerformance ip:industryPerformances)
			{
				if(ip.getId().getUsername().equals(user.getUsername()))
				{
					userObj.put("industryAmount",ip.getAmount()+" amount won for "+client.getIndustry());
					userObj.put("industryDeals",ip.getNoofdeals()+" deals won for "+client.getIndustry());
				}
			}

			userObj.put("btn","<a class=\"btn btn-raised  btn-primary btn-xs assign-delegate-btn\">Assign</a>");


			saArr.add(userObj);
		}


		JSONObject retObj = clientService.assignSalesrep(clientid,salesrep);

		retObj.put("performance",saArr);

		return  retObj;
		
	}

    @PreAuthorize("hasRole('BM')")
	@RequestMapping(value = "/assignSalesRepEdit/{salesrep}/{clientid}", method = RequestMethod.GET)
	public @ResponseBody JSONObject assignSalesRepEdit(@PathVariable("salesrep") String salesrep,@PathVariable("clientid") long clientid,HttpServletRequest request, Model model)
	{

		JSONObject retObj = clientService.assignSalesrep(clientid,salesrep);


		return  retObj;

	}


	@PreAuthorize("hasRole('BM')")
	@RequestMapping(value = "/assignDelegates/{clientid}", method = RequestMethod.POST)
	public @ResponseBody JSONObject assignSalesRep(@PathVariable("clientid") long clientid,@RequestBody JSONObject postData,
												   HttpServletRequest request, Model model) throws IOException {



		Client client =(Client) clientService.getClientById(clientid).get("data");

		String delegateStr = (String) postData.get("delegateStr");

		System.out.println(postData);


		if(client !=null)
		{

			String[] delegates = delegateStr.split(";");

			System.out.println(delegates.length);

			List<User> delegateSalesreps = new LinkedList<>();

			for(String salesRep:delegates)
			{
				User srep = userService.getUserByUsername(salesRep);

				if(srep != null)
					delegateSalesreps.add(srep);
			}


			client.setDelegates(delegateSalesreps);

			JSONObject retObj =  clientService.updateClient(client);

			if((boolean) retObj.get("success")) {
				for (User srep : delegateSalesreps) {
					notificationService.create("Delegate!", "You have been assigned as a delegate for client " + client.getName(),
							client.getId(), srep.getUsername(), "/getClientDetails/" + clientid);
				}


				return SbLibrary.getResponseObj(true, "Delegates assigned");
			}

			return retObj;
		}

		return SbLibrary.getResponseObj(false,"Couldn't fetch Client information");

	}
	@PreAuthorize("hasRole('BM')")
	@RequestMapping(value = "/getAvailableDelegates/{clientid}", method = RequestMethod.POST)
	public @ResponseBody JSONObject getAvailableDelegates(@PathVariable("clientid") long clientid) throws IOException {

		Client client =(Client) clientService.getClientById(clientid).get("data");

		if(client !=null)
		{
			List<User> allSa = userService.getAllSalesAssocs();

			JSONArray retArr = new JSONArray();

			for(User salesRep:allSa)
			{
				if(salesRep.getUsername().equals(client.getSrepresentative())) continue;
				retArr.add(salesRep);
			}
			return SbLibrary.getResponseObj(true,"Available Delegates",retArr);
		}

		return SbLibrary.getResponseObj(false,"Couldn't fetch Client information");

	}



	@RequestMapping(value = "/myPipeline", method = RequestMethod.GET)
	public String myPipeline(HttpServletRequest request, Model model)
	{
		JSONObject obj = clientService.getPipelineClients();

		if ((boolean) obj.get("success"))
		{
			model.addAttribute("data", obj.get("data"));
		}

		return SbLibrary.getPageFragment(request, "pipeline");
	}

	@RequestMapping(value = "/updateClientStatus/{id}", method = RequestMethod.POST)
	public @ResponseBody JSONObject updateStatus(@PathVariable("id") long id, @RequestBody JSONObject postData)
	{

		String newStatus = (String) postData.get("newstatus");


		Client update = new Client();

		update.setId(id);
		update.setStatus(ClientStatusType.valueOf(newStatus));

		return clientService.updateClient(update);

	}
	
	
	@RequestMapping(value="/updateClientStatus/{id}/{status}/{urlid}",method=RequestMethod.GET)
	public  String updateProposalStatus(@PathVariable long id, @PathVariable ClientStatusType status,@PathVariable long urlid)
	{

		Client update = new Client();

		update.setId(id);
		update.setStatus(status);

		clientService.updateClient(update);



        if(urlid == 1)
        {
            return SbLibrary.getRedirectUrl("/mySuspects","");

        }else if(urlid == 2)
        {
            return SbLibrary.getRedirectUrl("/unassignedProspects","");
        }

	
		return SbLibrary.getRedirectUrl("/getClientDetails/", id);
		
	}
	
	
	@RequestMapping(value="/mailClient/{clientid}",method=RequestMethod.GET)
	public  String mailProposal(@PathVariable long clientid, RedirectAttributes redAttr)
	{
		
		redAttr.addAttribute("mailtext", "");
		
		redAttr.addAttribute("backurl", SbLibrary.getUrl("/getClientDetails/", clientid));

		return SbLibrary.getRedirectUrl("/sendEmail/", clientid);
		
	}



	@RequestMapping(value = "/countMySuspects", method = RequestMethod.GET)
	public @ResponseBody JSONObject countMySuspects(HttpServletRequest request, Model model)
	{

		String username = SbLibrary.getCurrentUser().getUsername();

		return clientService.countMySuspects(username);
	}


	@RequestMapping(value = "/countUnassignedClients", method = RequestMethod.GET)
	public @ResponseBody JSONObject countUnassignedClients(HttpServletRequest request, Model model)
	{

		String username = SbLibrary.getCurrentUser().getUsername();

		return clientService.countUnassignedClients(username);
	}

	@RequestMapping(value = "/getClientInStages", method = RequestMethod.GET)
	public @ResponseBody JSONObject getClientInStages(HttpServletRequest request, Model model)
	{

		JSONObject obj = clientService.getMyClients();

		return obj;

	}

    @RequestMapping(value = "/salesTeam", method = RequestMethod.GET)
	public String getSalesTeam(HttpServletRequest request, Model model)
	{

		List<User> salesreps = userService.getAllSalesAssocs();


        model.addAttribute("salesreps",salesreps);

        return SbLibrary.getPageFragment(request, "salesreps");

	}




	@RequestMapping(value = "/createSuspect")
	public String createSuspectSaGet() {
		return "create-client";
	}

	@RequestMapping(value = "/createSuspect",method = RequestMethod.POST)
	public @ResponseBody JSONObject createSuspectSaPost(@RequestBody Client client, BindingResult bindingResult) {


        User cuUser = SbLibrary.getCurrentUser().getUser();


		if(!bindingResult.hasErrors())
		{
            JSONObject addObj;

            if(!cuUser.getRoles().equals("BM"))
            {
               addObj = clientService.addBySalesRep(client);
            }else
            {
                addObj = clientService.addByManager(client);
            }

			return addObj;
		}




        return SbLibrary.getResponseObj(false,"Form Submission Error",client);

	}

	@RequestMapping(value = "/getAllGmapids",method = RequestMethod.GET)
	public @ResponseBody JSONObject getAllGmapids() {



        return clientService.getAllGmapId();


	}

    @RequestMapping(value = "/mySuspects",method = RequestMethod.GET)
	public  String mySuspects(Model model) {

        String username = SbLibrary.getCurrentUser().getUsername();

        List<Client> suspects = (List<Client>) clientService.getMySuspects(username);

        model.addAttribute("data",suspects);

        return "my-suspects";
	}
	@RequestMapping(value = "/performanceByIndustry/{industry}",method = RequestMethod.GET)
	public @ResponseBody JSONObject performanceByIndustry(@PathVariable String industry) {


        List<IndustryPerformance> industryPerformances = industryPerformanceService.getPerformanceByIndustry(industry);

        List<User> allSalesAssocs = userService.getAllSalesAssocs();

        JSONArray saArr = new JSONArray();

        for(User user:allSalesAssocs)
        {
            JSONObject userObj = new JSONObject();
            userObj.put("user",user);
            userObj.put("clients",clientService.getNumberOfClients(user.getUsername()));
            userObj.put("stats",statsService.getStats(user.getUsername()));

            userObj.put("industryAmount",0+" amount won for "+industry);
            userObj.put("industryDeals",0+" deals won for "+industry);

//            System.out.println(industryPerformances);

            for( IndustryPerformance ip:industryPerformances)
            {
                if(ip.getId().getUsername().equals(user.getUsername()))
                {
                    userObj.put("industryAmount",ip.getAmount()+" amount won for "+industry);
                    userObj.put("industryDeals",ip.getNoofdeals()+" deals won for "+industry);
                }
            }

            userObj.put("btn","<a class=\"btn btn-raised  btn-primary btn-xs assign-btn\">Assign</a>");


            saArr.add(userObj);
        }



        return SbLibrary.getResponseObj(true,"performance for "+industry,saArr);


	}





}
