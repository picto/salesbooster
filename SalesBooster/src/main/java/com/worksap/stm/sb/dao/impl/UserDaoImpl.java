package com.worksap.stm.sb.dao.impl;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.worksap.stm.sb.dao.spec.UserDao;
import com.worksap.stm.sb.dto.RoleDto;
import com.worksap.stm.sb.dto.TeamDto;
import com.worksap.stm.sb.dto.UserDto;

@Repository
public class UserDaoImpl implements UserDao {

	@Autowired
	private JdbcTemplate template;

	
	private final static String USER_ACCOUNT_TABLE_NAME = "user_account";
	private final static String USER_ROLE_TABLE_NAME = "user_role";
	private final static String ROLE_TABLE_NAME = "role";
	private final static String TEAM_TABLE_NAME = "team";
	private final static String USER_TEAM_TABLE_NAME = "user_team";
	
	
	private static final StringBuilder  GET_ALL_USERS = new StringBuilder("SELECT * FROM ").append(USER_ACCOUNT_TABLE_NAME);
	private static final StringBuilder  GET_ALL_USER_BY_USERNAME = new StringBuilder("SELECT * FROM ").append(USER_ACCOUNT_TABLE_NAME).append(" WHERE username = ?");
	private static final StringBuilder  GET_ROLES = new StringBuilder("SELECT * FROM ").append(USER_ROLE_TABLE_NAME).append(" WHERE username = ?");
	private static final StringBuilder  GET_TEAMS = new StringBuilder("SELECT * FROM ").append(TEAM_TABLE_NAME)
			.append(" as a INNER JOIN ").append(USER_TEAM_TABLE_NAME).append(" as b on a.id = b.team_id ").append(" WHERE ").append("b.username = ?");
	

	

	
	@Override
	public List<UserDto> getUsers(String cond) throws IOException {
		// TODO Auto-generated method stub
		try
		{
			return template.query(
					GET_ALL_USERS.toString(),					
					(rs, rownum) -> {
						return new UserDto(
								rs.getString("id"), 
								rs.getString("username"), 
								rs.getString("password"), 
								rs.getString("name"), 
								rs.getString("email"),
								rs.getString("email_pass"));
					});
		}catch (DataAccessException e) {
			throw new IOException(e);
		}
	}
	
	
	@Override
	public UserDto getUserByUsername(String username) throws IOException {
		// TODO Auto-generated method stub
		try
		{
			return template.queryForObject(
					GET_ALL_USER_BY_USERNAME.toString(),					
					(rs, rownum) -> {
						return new UserDto(
								rs.getString("id"), 
								rs.getString("username"), 
								rs.getString("password"), 
								rs.getString("name"), 
								rs.getString("email"),
								rs.getString("email_pass"));
					},username);
		}catch (DataAccessException e) {
			throw new IOException(e);
		}
	}



	@Override
	public List<RoleDto> getRoles(String username) throws IOException {
		// TODO Auto-generated method stub
		try
		{
			return template.query(
					GET_ROLES.toString(),					
					(rs, rownum) -> {
						return new RoleDto(rs.getString("role"));
					},username);
		}catch (DataAccessException e) {
			throw new IOException(e);
		}
	}

	@Override
	public List<TeamDto> getTeams(String username) throws IOException {
		// TODO Auto-generated method stub
		try
		{
			return template.query(
					GET_TEAMS.toString(),					
					(rs, rownum) -> {
						return new TeamDto(rs.getString("team_name"));
					},username);
		}catch (DataAccessException e) {
			throw new IOException(e);
		}
	}

	
}
